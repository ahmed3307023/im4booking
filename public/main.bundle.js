webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/@angular/platform-browser/animations.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("../../../material/@angular/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_io_datepicker__ = __webpack_require__("../../../../angular-io-datepicker/src/datepicker/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_io_datepicker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular_io_datepicker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular_io_overlay__ = __webpack_require__("../../../../angular-io-overlay/src/overlay/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular_io_overlay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_angular_io_overlay__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login_component__ = __webpack_require__("../../../../../src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__navigation_navigation_component__ = __webpack_require__("../../../../../src/app/navigation/navigation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__news_news_component__ = __webpack_require__("../../../../../src/app/news/news.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__offer_offer_component__ = __webpack_require__("../../../../../src/app/offer/offer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__packages_packages_component__ = __webpack_require__("../../../../../src/app/packages/packages.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__feedback_feedback_component__ = __webpack_require__("../../../../../src/app/feedback/feedback.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__users_users_component__ = __webpack_require__("../../../../../src/app/users/users.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__feedback_feedback_service__ = __webpack_require__("../../../../../src/app/feedback/feedback.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__offer_offer_service__ = __webpack_require__("../../../../../src/app/offer/offer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__termsandconditions_termsandconditions_service__ = __webpack_require__("../../../../../src/app/termsandconditions/termsandconditions.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__packages_packages_service__ = __webpack_require__("../../../../../src/app/packages/packages.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__termsandconditions_termsandconditions_component__ = __webpack_require__("../../../../../src/app/termsandconditions/termsandconditions.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__news_news_service__ = __webpack_require__("../../../../../src/app/news/news.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__requests_requests_component__ = __webpack_require__("../../../../../src/app/requests/requests.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__requests_requests_service__ = __webpack_require__("../../../../../src/app/requests/requests.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__users_user_service__ = __webpack_require__("../../../../../src/app/users/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__password_redirect_password_redirect_component__ = __webpack_require__("../../../../../src/app/password-redirect/password-redirect.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29_ng_zorro_antd__ = __webpack_require__("../../../../ng-zorro-antd/esm5/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







































var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_9__login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_10__navigation_navigation_component__["a" /* NavigationComponent */],
            __WEBPACK_IMPORTED_MODULE_11__news_news_component__["a" /* NewsComponent */],
            __WEBPACK_IMPORTED_MODULE_12__dashboard_dashboard_component__["a" /* DashboardComponent */],
            __WEBPACK_IMPORTED_MODULE_13__offer_offer_component__["a" /* OfferComponent */],
            __WEBPACK_IMPORTED_MODULE_14__packages_packages_component__["a" /* PackagesComponent */],
            __WEBPACK_IMPORTED_MODULE_15__feedback_feedback_component__["a" /* FeedbackComponent */],
            __WEBPACK_IMPORTED_MODULE_16__users_users_component__["a" /* UsersComponent */],
            __WEBPACK_IMPORTED_MODULE_23__termsandconditions_termsandconditions_component__["a" /* TermsandconditionsComponent */],
            __WEBPACK_IMPORTED_MODULE_25__requests_requests_component__["a" /* RequestsComponent */],
            __WEBPACK_IMPORTED_MODULE_28__password_redirect_password_redirect_component__["a" /* PasswordRedirectComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_21__angular_common_http__["a" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_20__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["ReactiveFormsModule"],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["a" /* MdCardModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["b" /* MdButtonModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["c" /* MdTabsModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["d" /* MdInputModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["e" /* MdFormFieldModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["f" /* MdDatepickerModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["g" /* MdSelectModule */],
            __WEBPACK_IMPORTED_MODULE_20__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["h" /* MdCheckboxModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["i" /* MdNativeDateModule */],
            __WEBPACK_IMPORTED_MODULE_6_angular_io_datepicker__["DatePickerModule"],
            __WEBPACK_IMPORTED_MODULE_7_angular_io_overlay__["OverlayModule"],
            __WEBPACK_IMPORTED_MODULE_29_ng_zorro_antd__["a" /* NgZorroAntdModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* RouterModule */].forRoot([
                {
                    path: 'news',
                    component: __WEBPACK_IMPORTED_MODULE_11__news_news_component__["a" /* NewsComponent */]
                },
                {
                    path: 'login',
                    component: __WEBPACK_IMPORTED_MODULE_9__login_login_component__["a" /* LoginComponent */]
                },
                {
                    path: '',
                    redirectTo: '/login',
                    pathMatch: 'full'
                },
                {
                    path: 'nav',
                    component: __WEBPACK_IMPORTED_MODULE_10__navigation_navigation_component__["a" /* NavigationComponent */]
                },
                {
                    path: 'offer',
                    component: __WEBPACK_IMPORTED_MODULE_13__offer_offer_component__["a" /* OfferComponent */]
                },
                {
                    path: 'packages',
                    component: __WEBPACK_IMPORTED_MODULE_14__packages_packages_component__["a" /* PackagesComponent */]
                },
                {
                    path: 'feedback',
                    component: __WEBPACK_IMPORTED_MODULE_15__feedback_feedback_component__["a" /* FeedbackComponent */]
                },
                {
                    path: 'users',
                    component: __WEBPACK_IMPORTED_MODULE_16__users_users_component__["a" /* UsersComponent */]
                },
                {
                    path: 'term',
                    component: __WEBPACK_IMPORTED_MODULE_23__termsandconditions_termsandconditions_component__["a" /* TermsandconditionsComponent */]
                },
                {
                    path: 'requests',
                    component: __WEBPACK_IMPORTED_MODULE_25__requests_requests_component__["a" /* RequestsComponent */]
                },
                {
                    path: 'passRedirect',
                    component: __WEBPACK_IMPORTED_MODULE_28__password_redirect_password_redirect_component__["a" /* PasswordRedirectComponent */]
                }
            ])
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_17__feedback_feedback_service__["a" /* FeedbackService */], __WEBPACK_IMPORTED_MODULE_18__offer_offer_service__["a" /* OfferService */], __WEBPACK_IMPORTED_MODULE_20__angular_http__["a" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_21__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_22__packages_packages_service__["a" /* PackagesService */], __WEBPACK_IMPORTED_MODULE_24__news_news_service__["a" /* NewsService */], __WEBPACK_IMPORTED_MODULE_19__termsandconditions_termsandconditions_service__["a" /* TermsandconditionsService */], __WEBPACK_IMPORTED_MODULE_26__requests_requests_service__["a" /* RequestService */], __WEBPACK_IMPORTED_MODULE_27__users_user_service__["a" /* UserService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>\n<div>\n  <h1></h1>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardComponent = (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-dashboard',
        template: __webpack_require__("../../../../../src/app/dashboard/dashboard.component.html"),
        styles: [__webpack_require__("../../../../../src/app/dashboard/dashboard.component.css")]
    }),
    __metadata("design:paramtypes", [])
], DashboardComponent);

//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ "../../../../../src/app/feedback/feedback.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>\n<ul class=\"cards\">\n  <li class=\"cards__item\" *ngFor=\"let feedback of feedbacks\">\n    <div class=\"card\">\n      <div class=\"card__content\">\n        <div class=\"card__title\"> {{feedback.about}} </div>\n        <p class=\"card__text\">{{feedback.details}} </p>\n        <button class=\"btn btn--block card__btn\" (click)=\"delete(feedback._id)\" >DELETE</button>\n      </div>\n    </div>\n  </li>\n</ul>\n"

/***/ }),

/***/ "../../../../../src/app/feedback/feedback.component.less":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "*,\n*::before,\n*::after {\n  box-sizing: border-box;\n}\nhtml {\n  background-color: #f0f0f0;\n}\nbody {\n  color: #999999;\n  font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  font-style: normal;\n  font-weight: 200;\n  letter-spacing: 0;\n  padding: 1rem;\n  text-rendering: optimizeLegibility;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  -moz-font-feature-settings: \"liga\" on;\n}\nimg {\n  height: auto;\n  max-width: 100%;\n  vertical-align: middle;\n}\n.btn {\n  background-color: white;\n  border: 1px solid #cccccc;\n  color: #2e25e7;\n  padding: 0.5rem;\n  font-weight: 300;\n}\n.btn:hover .btn {\n  -webkit-filter: contrast(10%);\n          filter: contrast(10%);\n}\ninput {\n  font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  font-style: normal;\n  font-weight: 400;\n  font-weight: 300;\n  border: 0;\n  border-bottom: 1px solid #2e25e7;\n  width: 100%;\n  height: 36px;\n  font-size: 26px;\n}\ninput:focus {\n  outline: none;\n  box-shadow: none;\n  background: #5f6de7;\n}\n.wrap {\n  padding: 120px 0;\n  font-size: 22px;\n  color: #888;\n  width: 400px;\n  font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  font-style: normal;\n  font-weight: 400;\n  margin: 0 auto;\n  text-align: center;\n}\n.in {\n  width: 30%;\n  padding: 30px;\n  text-align: center;\n  -ms-flex-item-align: center;\n      -ms-grid-row-align: center;\n      align-self: center;\n}\ntextarea {\n  height: auto;\n  max-width: 600px;\n  color: #999;\n  font-weight: 400;\n  font-size: 30px;\n  font-family: 'Ubuntu', Helvetica, Arial, sans-serif;\n  width: 100%;\n  background: #fff;\n  border-radius: 3px;\n  line-height: 2em;\n  border: none;\n  box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.1);\n  padding: 30px;\n  transition: height 2s ease;\n}\n.wrapt {\n  height: 50 px;\n  padding: 120px 0;\n  font-size: 20px;\n  color: #888;\n  width: 200px;\n  font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  font-style: normal;\n  font-weight: 200;\n  margin: 0 auto;\n  text-align: center;\n}\n.body {\n  font-family: Lato;\n  width: 100%;\n}\n.btn--block {\n  display: block;\n  width: 100%;\n}\n.cards {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  list-style: none;\n  margin: 0;\n  padding: 0;\n  color: #999999;\n  font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  font-style: normal;\n  font-weight: 400;\n  letter-spacing: 0;\n  text-rendering: optimizeLegibility;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  -moz-font-feature-settings: \"liga\" on;\n}\n.cards__item {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 1rem;\n}\n@media (min-width: 40rem) {\n  .cards__item {\n    width: 50%;\n  }\n}\n@media (min-width: 56rem) {\n  .cards__item {\n    width: 33.3333%;\n  }\n}\n.card {\n  background-color: white;\n  border-radius: 0.25rem;\n  box-shadow: 0 20px 40px -14px rgba(0, 0, 0, 0.25);\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  overflow: hidden;\n}\n.card:hover .card__image {\n  -webkit-filter: contrast(100%);\n          filter: contrast(100%);\n}\n.card__content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 1rem;\n}\n.card__image {\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  border-top-left-radius: 0.25rem;\n  border-top-right-radius: 0.25rem;\n  -webkit-filter: contrast(70%);\n          filter: contrast(70%);\n  overflow: hidden;\n  position: relative;\n  transition: -webkit-filter 0.5s cubic-bezier(0.43, 0.41, 0.22, 0.91);\n  transition: filter 0.5s cubic-bezier(0.43, 0.41, 0.22, 0.91);\n  transition: filter 0.5s cubic-bezier(0.43, 0.41, 0.22, 0.91), -webkit-filter 0.5s cubic-bezier(0.43, 0.41, 0.22, 0.91);\n  /*&::before {\n   content: \"\";\n   display: block;\n   padding-top: 56.25%; // 16:9 aspect ratio\n }*/\n}\n@media (min-width: 40rem) {\n  .card__image::before {\n    padding-top: 66.6%;\n  }\n}\n.card__title {\n  color: #2e25e7;\n  font-size: 1.25rem;\n  font-weight: 300;\n  letter-spacing: 2px;\n  text-transform: uppercase;\n}\n.card__text {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  font-size: 0.875rem;\n  line-height: 1.5;\n  margin-bottom: 1.25rem;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/feedback/feedback.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__feedback_service__ = __webpack_require__("../../../../../src/app/feedback/feedback.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FeedbackComponent = (function () {
    function FeedbackComponent(service) {
        this.service = service;
    }
    FeedbackComponent.prototype.ngOnInit = function () {
        this.viewFeedbacks();
    };
    FeedbackComponent.prototype.viewFeedbacks = function () {
        var _this = this;
        this.service.getFeedbacks().then(function (res) {
            _this.esponse = JSON.parse(res)['response'];
            if (JSON.parse(res)['valid']) {
                _this.feedbacks = _this.esponse;
            }
            else {
            }
        });
    };
    FeedbackComponent.prototype.delete = function (name) {
        console.log(name);
        this.service.delFeedbacks(name);
    };
    return FeedbackComponent;
}());
FeedbackComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-feedback',
        template: __webpack_require__("../../../../../src/app/feedback/feedback.component.html"),
        styles: [__webpack_require__("../../../../../src/app/feedback/feedback.component.less")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__feedback_service__["a" /* FeedbackService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__feedback_service__["a" /* FeedbackService */]) === "function" && _a || Object])
], FeedbackComponent);

var _a;
//# sourceMappingURL=feedback.component.js.map

/***/ }),

/***/ "../../../../../src/app/feedback/feedback.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FeedbackService = (function () {
    function FeedbackService(http, httpPoster) {
        this.http = http;
        this.httpPoster = httpPoster;
        this.valid = true;
        this.header = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]();
    }
    FeedbackService.prototype.getFeedbacks = function () {
        // this.header.append('Content-Type', 'application/json' );
        // this.header.append('Access-Control-Allow-Origin', '*' );
        var url = "im4booking/feedback";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var hh = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        hh.append('ahgjjkgfjgfjghfhgfhgfjhgfhjgfghjfjhfjhgfjhgfhjfjhfhjgss', 'asdsasd');
        hh.append('Content-Type', 'application/json');
        hh.append('Access-Control-Allow-Origin', '*');
        hh.append('Access-Control-Allow-Headers', '*');
        hh.append('Access-Control-Allow-Methods', '*');
        hh.append('Access-Control-Allow-Credentials', '*');
        hh.append('Access-Control-Allow-Request-Headers', '*');
        hh.append('Access-Control-Allow-Request-Methods', '*');
        hh.append('Access-Control-Origin', '*');
        hh.append('Accept', 'application/json');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: hh
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            return response['_body'];
        });
    };
    FeedbackService.prototype.delFeedbacks = function (id) {
        var _this = this;
        this.header.append('Content-Type', 'application/json');
        this.header.append('Access-Control-Allow-Origin', '*');
        var url = "im4booking/feedback/delete";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        this.httpPoster.post(url, {
            'user_id': user_id,
            'api_key': api_key,
            'feedback_id': id,
        }, {
            headers: this.header
        }).subscribe(function (data) {
            _this.valid = data.valid;
            _this.err = data.msg;
        });
    };
    return FeedbackService;
}());
FeedbackService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */]) === "function" && _b || Object])
], FeedbackService);

var _a, _b;
//# sourceMappingURL=feedback.service.js.map

/***/ }),

/***/ "../../../../../src/app/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Karla);", ""]);
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Lato);", ""]);

// module
exports.push([module.i, "body{\n  margin:0;\n  color:#6a6f8c;\n  background:#c8c8c8;\n\n}\n*,:after,:before{box-sizing:border-box}\n.clearfix:after,.clearfix:before{content:'';display:table}\n.clearfix:after{clear:both;display:block}\na{color:inherit;text-decoration:none}\n\n.login-wrap{\n  width:100%;\n  margin:auto;\n  max-width:525px;\n  min-height:670px;\n  position:relative;\n  background:url(https://raw.githubusercontent.com/khadkamhn/day-01-login-form/master/img/bg.jpg) no-repeat center;\n  box-shadow:0 12px 15px 0 rgba(0,0,0,.24),0 17px 50px 0 rgba(0,0,0,.19);\n  font-family: 'Lato';\n}\n.login-html{\n  width:100%;\n  height:100%;\n  position:absolute;\n  padding:90px 70px 50px 70px;\n  background:rgba(40,57,101,.9);\n}\n.login-html .sign-in-htm,\n.login-html .sign-up-htm{\n  top:0;\n  left:0;\n  right:0;\n  bottom:0;\n  position:absolute;\n  -webkit-transform:rotateY(180deg);\n          transform:rotateY(180deg);\n  -webkit-backface-visibility:hidden;\n          backface-visibility:hidden;\n  transition:all .4s linear;\n}\n.login-html .sign-in,\n.login-html .sign-up,\n.login-form .group .check{\n  display:none;\n}\n.login-html .tab,\n.login-form .group .label,\n.login-form .group .button{\n  text-transform:uppercase;\n}\n.login-html .tab{\n  font-size:22px;\n  margin-right:15px;\n  padding-bottom:5px;\n  margin:0 15px 10px 0;\n  display:inline-block;\n  border-bottom:2px solid transparent;\n}\n.login-html .sign-in:checked + .tab,\n.login-html .sign-up:checked + .tab{\n  color:#fff;\n  border-color:#1161ee;\n}\n.login-form{\n  min-height:345px;\n  position:relative;\n  -webkit-perspective:1000px;\n          perspective:1000px;\n  -webkit-transform-style:preserve-3d;\n          transform-style:preserve-3d;\n}\n.login-form .group{\n  margin-bottom:15px;\n}\n.login-form .group .label,\n.login-form .group .input,\n.login-form .group .button{\n  width:100%;\n  color:#fff;\n  display:block;\n}\n.login-form .group .input,\n.login-form .group .button{\n  border:none;\n  padding:15px 20px;\n  border-radius:25px;\n  background:rgba(255,255,255,.1);\n}\n.login-form .group input[data-type=\"password\"]{\n  text-security:circle;\n  -webkit-text-security:circle;\n}\n.login-form .group .label{\n  color:#aaa;\n  font-size:12px;\n}\n.login-form .group .button{\n  background:#1161ee;\n}\n.login-form .group label .icon{\n  width:15px;\n  height:15px;\n  border-radius:2px;\n  position:relative;\n  display:inline-block;\n  background:rgba(255,255,255,.1);\n}\n.login-form .group label .icon:before,\n.login-form .group label .icon:after{\n  content:'';\n  width:10px;\n  height:2px;\n  background:#fff;\n  position:absolute;\n  transition:all .2s ease-in-out 0s;\n}\n.login-form .group label .icon:before{\n  left:3px;\n  width:5px;\n  bottom:6px;\n  -webkit-transform:scale(0) rotate(0);\n          transform:scale(0) rotate(0);\n}\n.login-form .group label .icon:after{\n  top:6px;\n  right:0;\n  -webkit-transform:scale(0) rotate(0);\n          transform:scale(0) rotate(0);\n}\n.login-form .group .check:checked + label{\n  color:#fff;\n}\n.login-form .group .check:checked + label .icon{\n  background:#1161ee;\n}\n.login-form .group .check:checked + label .icon:before{\n  -webkit-transform:scale(1) rotate(45deg);\n          transform:scale(1) rotate(45deg);\n}\n.login-form .group .check:checked + label .icon:after{\n  -webkit-transform:scale(1) rotate(-45deg);\n          transform:scale(1) rotate(-45deg);\n}\n.login-html .sign-in:checked + .tab + .sign-up + .tab + .login-form .sign-in-htm{\n  -webkit-transform:rotate(0);\n          transform:rotate(0);\n}\n.login-html .sign-up:checked + .tab + .login-form .sign-up-htm{\n  -webkit-transform:rotate(0);\n          transform:rotate(0);\n}\n\n.hr{\n  height:2px;\n  margin:60px 0 50px 0;\n  background:rgba(255,255,255,.2);\n}\n.foot-lnk{\n  text-align:center;\n}\n.back {\n  background-color: #1475ff;\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  top: 0;\n  z-index: 10;\n\n}\n\n.button {\n  opacity: 0.8;\n}\n\n.button:hover {\n  opacity: 1;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"back\" *ngIf=\"show\">\n  <div class=\"login-wrap\">\n    <div class=\"login-html\">\n      <input id=\"tab-1\" type=\"radio\" name=\"tab\" class=\"sign-in\" checked><label for=\"tab-1\" class=\"tab\">Sign In</label>\n      <input id=\"tab-2\" type=\"radio\" name=\"tab\" class=\"sign-up\"><label for=\"tab-2\" class=\"tab\"></label>\n      <div class=\"login-form\">\n        <div class=\"sign-in-htm\">\n          <div class=\"group\">\n            <label for=\"user\" class=\"label\" >Username</label>\n            <input id=\"user\" type=\"text\" class=\"input\" #username>\n          </div>\n          <div class=\"group\">\n            <label for=\"pass\" class=\"label\" >Password</label>\n            <input id=\"pass\" type=\"password\" class=\"input\" data-type=\"password\" #password>\n          </div>\n          <div class=\"group\">\n            <button (click)=\"login(username.value, password.value)\" class=\"button\" value=\"Sign In\">Sign In</button>\n          </div>\n          <div class=\"group\" *ngIf=\"showWrongUser\">\n            <label>Wrong Username Or Password</label>\n          </div>\n          <div class=\"hr\"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = (function () {
    function LoginComponent(http, router) {
        this.http = http;
        this.router = router;
        this.show = true;
        this.showWrongUser = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.login = function (username, password) {
        this.loginService(username, password);
    };
    LoginComponent.prototype.route = function () {
        this.router.navigateByUrl("offer");
    };
    //user_name
    LoginComponent.prototype.loginService = function (username, password) {
        var _this = this;
        var url = '/im4booking/user/login';
        this.http.post(url, {
            'user_name': username,
            'password': password
        }).subscribe(function (res) {
            console.log(res);
            var body = JSON.parse(res['_body']);
            console.log(body);
            if (body['valid'] == true) {
                var login_res = body['response'];
                console.log(login_res);
                localStorage.setItem('user_id', login_res['user_id']);
                localStorage.setItem('api_key', login_res['api_key']);
                if (login_res['login_status'] == 'new login') {
                    console.log("Logiiii");
                    _this.router.navigateByUrl("passRedirect");
                }
                else {
                    _this.router.navigateByUrl("offer");
                }
            }
            else {
                console.log("in else");
                _this.showWrongUser = true;
            }
        });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-login',
        template: __webpack_require__("../../../../../src/app/login/login.component.html"),
        styles: [__webpack_require__("../../../../../src/app/login/login.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _b || Object])
], LoginComponent);

var _a, _b;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../src/app/navigation/navigation.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Lato:300,500);", ""]);

// module
exports.push([module.i, "/*\n  Simple CSS navigation bar with fading inactive links\n*/\n\n/*\n  pad the body and set default font styles\n*/\nbody {\n   /*Firefox*/\n  display:-moz-box;\n  -moz-box-pack:center;\n  -moz-box-align:center;\nbackground-color: #ffff;\n  /* Safari and Chrome */\n  display:-webkit-box;\n  -webkit-box-pack:center;\n  -webkit-box-align:center;\n\n  font: 300 40px/1.2 Lato;\n}\n\n/*\n  navigation\n*/\n.nav {\n  list-style: none;\n}\n\n/*\n  nav list items\n  1. side by side\n  2. needed for circle positioning\n*/\n.nav li {\n  float: left; /*1*/\n  display: inline;\n  margin: auto;\n\n}\n\n/*\n  nav link items\n*/\n.nav > li a {\n  display: block; /*1*/\n  padding: 12px 18px; /*2*/\n  text-decoration: none; /*3*/\n  color: #3a08e7; /*4*/\n  border-bottom: 1px solid #eee;\n  transition: all ease .5s;\n}\n\n/*\n  fade out all links on ul hover\n*/\n.nav:hover > li a {\n  opacity: .5;\n  transition: all ease .5s;\n}\n\n/*\n  override previous rule to highlight current link\n*/\n.nav > li:hover a {\n  opacity: 1;\n  color: #3a08e7;\n  border-color: #2e25e7;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/navigation/navigation.component.html":
/***/ (function(module, exports) {

module.exports = "<body>\n<ul class=\"nav\">\n  <li><a routerLink=\"/offer\">Offer</a><i class=\"circle\"></i></li>\n  <li><a routerLink=\"/packages\">Packages</a></li>\n  <li><a routerLink=\"/feedback\">Feedback</a></li>\n  <li><a routerLink=\"/news\">News</a></li>\n  <li><a routerLink=\"/users\">Users</a></li>\n  <li><a routerLink=\"/term\">About us</a></li>\n  <li><a routerLink=\"/requests\">Requests</a></li>\n</ul>\n<br/>\n<br/>\n</body>\n"

/***/ }),

/***/ "../../../../../src/app/navigation/navigation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavigationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavigationComponent = (function () {
    function NavigationComponent() {
        this.showNews = false;
    }
    NavigationComponent.prototype.ngOnInit = function () {
    };
    NavigationComponent.prototype.showNewsComponent = function () {
        this.showNews = true;
    };
    return NavigationComponent;
}());
NavigationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-navigation',
        template: __webpack_require__("../../../../../src/app/navigation/navigation.component.html"),
        styles: [__webpack_require__("../../../../../src/app/navigation/navigation.component.css")]
    }),
    __metadata("design:paramtypes", [])
], NavigationComponent);

//# sourceMappingURL=navigation.component.js.map

/***/ }),

/***/ "../../../../../src/app/news/news.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>\n<body>\n  <md-tab-group>\n    <md-tab label=\"VIEW NEWS\">\n        <button class=\"btn btn-primary\" (click)=\"sumbitdateclicked()\">REFRESH</button>\n      <div class=\"cards-tab\">\n        <h1 class=\"view-title\">Available News</h1>\n        <md-card class=\"package-card\" *ngFor=\"let offer of offersArray\">\n          <md-card-header>\n          </md-card-header>\n          <md-card-content>\n\n            <div>\n                <div><img src=\"{{offer.img_path}}\"></div>\n                <label class=\"label-view\">title:</label>\n                <h4 class=\"data-view\">{{offer.title[1].value}} </h4>\n                <h4 class=\"data-view\" dir=\"rtl\" lang=\"ar\">{{offer.title[0].value}}</h4>\n                <label class=\"label-view\">Description:</label>\n                <h4 class=\"data-view\">{{offer.body[1].value}}</h4>\n                <h4 class=\"data-view\" dir=\"rtl\" lang=\"ar\">{{offer.body[0].value}}</h4>\n            </div>\n\n              <div class=\"container\" *ngIf=\"offer.editingNow\">\n                <form (ngSubmit)=\"editsumbit()\"  [formGroup]=\"editVoucherForm\">\n                  <div class=\"form-group\">\n                    <label>Title :</label>\n                    <input type=\"text\" class=\"form-control\" id=\"titleng\" formControlName=\"titleEnglish\" value=\"{{offer.title[1].value}}\"  >\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Title Arabic:</label>\n                    <input type=\"text\" class=\"form-control\" id=\"titlearr\" formControlName=\"titleArabic\"  value=\"{{offer.title[0].value}}\">\n                  </div>\n                  <div class=\"form-group\">\n                    <label for=\"pwd\">Description :</label>\n                    <textarea class=\"form-control\" id=\"descriptioneng\" formControlName=\"descriptionEnglish\" value=\"{{offer.body[1].value}}\" ></textarea>\n                  </div>\n                  <div class=\"form-group\">\n                    <label for=\"pwd\">Description Arabic:</label>\n                    <textarea class=\"form-control\" id=\"descriptionarr\" formControlName=\"descriptionArabic\" value=\"{{offer.body[0].value}}\"></textarea>\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Image :</label>\n                    <input type=\"file\" class=\"form-control\" id=\"img\" (change)=\"pdfHandler($event)\">\n                  </div>\n                  <button class=\"btn btn-primary\" type=\"submit\">Submit</button>\n\n                </form>\n              </div>\n\n\n\n\n          </md-card-content>\n          <md-card-actions class=\"card-actions\">\n            <button class=\"edit-button\" md-button (click)=\"edit(offer._id)\">EDIT </button>\n            <button class=\"delete-button\" (click)=\"delete(offer._id)\" md-button>DELETE</button>\n          </md-card-actions>\n\n        </md-card>\n\n      </div>\n\n\n    </md-tab>\n\n\n    <md-tab label=\"ADD NEW NEWS\">\n      <h1 class=\"view-title\">Add new News</h1>\n      <div class=\"container\">\n        <form  (ngSubmit)=\"createOffer()\" [formGroup]=\"voucherForm\">\n          <div class=\"form-group\">\n            <label>Title :</label>\n            <input type=\"text\" class=\"form-control\" id=\"titleng\" formControlName=\"titleEnglish\">\n          </div>\n          <div class=\"form-group\">\n            <label>Title Arabic:</label>\n            <input type=\"text\" class=\"form-control\" id=\"titlearr\" formControlName=\"titleArabic\">\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pwd\">Description :</label>\n            <textarea class=\"form-control\" id=\"descriptioneng\" formControlName=\"descriptionEnglish\"></textarea>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pwd\">Description Arabic:</label>\n            <textarea class=\"form-control\" id=\"descriptionarr\" formControlName=\"descriptionArabic\"></textarea>\n          </div>\n          <div class=\"form-group\">\n            <label>Image :</label>\n            <input type=\"file\" class=\"form-control\" id=\"img\" (change)=\"pdfHandler($event)\">\n          </div>\n          <button class=\"btn btn-primary\" type=\"submit\">Submit</button>\n          <label>{{err}}</label>\n\n        </form>\n      </div>\n    </md-tab>\n  </md-tab-group>\n</body>\n"

/***/ }),

/***/ "../../../../../src/app/news/news.component.less":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Lato:300,500);", ""]);

// module
exports.push([module.i, "body {\n  font-family: Lato;\n  width: 100%;\n}\n.cards-tab {\n  margin: auto;\n  width: 75%;\n}\n.delete-button {\n  color: white;\n  background-color: #d50000;\n}\n.card-actions {\n  text-align: right;\n}\n.view-title {\n  font-size: 40px;\n  font-weight: bolder;\n  text-align: center;\n}\n.card-content {\n  text-align: left;\n  margin: auto;\n}\n.main-field {\n  margin: auto;\n  font-family: Lato;\n  font-size: 16px;\n  font-weight: bold;\n  height: 30px;\n  width: 100%;\n  border: none;\n  border-bottom: 2px solid #3a08e7;\n}\nth {\n  text-align: left;\n  padding-top: .5em;\n  padding-bottom: .5em;\n  width: auto;\n}\n.diva {\n  height: 50%;\n  width: 50%;\n}\n.titlo {\n  font-family: Lato;\n  font-size: 30px;\n}\ntr {\n  margin: 15px;\n  padding: 15px;\n}\ntd {\n  width: 100%;\n  text-align: justify;\n}\ntable {\n  width: 80%;\n  font-family: Lato;\n  font-size: 16px;\n  margin: auto;\n  text-align: center;\n}\n.form-actions {\n  text-align: right;\n}\n.form-actions button {\n  background-color: #3a08e7;\n  color: white;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/news/news.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__news_service__ = __webpack_require__("../../../../../src/app/news/news.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__news__ = __webpack_require__("../../../../../src/app/news/news.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewsComponent = (function () {
    function NewsComponent(offerService) {
        this.offerService = offerService;
        this.filter = false;
        this.filesb = false;
        this.divVisable = false;
        this.called = false;
        this.visable = false;
    }
    NewsComponent.prototype.ngOnInit = function () {
        this.viewOffers(this.neededDate);
        this.voucherForm = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormGroup"]({
            titleEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            titleArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            descriptionEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            descriptionArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            conditionEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            conditionArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            package: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            startDate: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            expiryDate: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            oneUse: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            points: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required)
        });
        this.editVoucherForm = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormGroup"]({
            titleEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            titleArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            descriptionEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            descriptionArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            conditionEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            conditionArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            package: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            startDate: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            oneUse: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            points: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            newexpiryDate: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
        });
    };
    NewsComponent.prototype.viewOffers = function (date) {
        var _this = this;
        this.offerService.getOffers(date).then(function (res) {
            _this.esponse = JSON.parse(res)['response'];
            if (JSON.parse(res)['valid']) {
                _this.offersArray = _this.esponse;
                console.log(_this.offersArray);
                _this.offersArray = _this.offersArray.map(function (offer) {
                    if (offer.img_path == "" || offer.img_path == null || offer.img_path == "TO BE uploaded") {
                        offer.img_path = "/news.png";
                    }
                    offer.editingNow = false;
                    return offer;
                });
            }
            console.log(_this.offersArray);
        });
    };
    NewsComponent.prototype.sumbitdateclicked = function () {
        this.neededDate = new Date(Date.now());
        console.log(this.neededDate);
        this.viewOffers(this.neededDate);
    };
    NewsComponent.prototype.viewOffer = function (id) {
        var _this = this;
        this.visable = true;
        this.editedOfferId = id;
        console.log(this.editedOfferId);
        this.offerService.getOffer(id).then(function (res) {
            _this.esponse = JSON.parse(res)['response'];
            if (JSON.parse(res)['valid']) {
                _this.viewedOffer = _this.esponse['data'];
            }
            else {
            }
        });
    };
    NewsComponent.prototype.createOffer = function () {
        this.creationDate = new Date(Date.now());
        this.addedOffer = new __WEBPACK_IMPORTED_MODULE_2__news__["a" /* News */](this.voucherForm.value.titleEnglish, this.voucherForm.value.titleArabic, this.voucherForm.value.descriptionEnglish, this.voucherForm.value.descriptionArabic, this.creationDate);
        console.log(this.addedOffer);
        console.log("test");
        this.filesb = true;
        this.offerService.addOffer(this.addedOffer);
        this.err = this.offerService.err;
        this.voucherForm.reset();
    };
    NewsComponent.prototype.edit = function (id) {
        var _this = this;
        this.divVisable = true;
        this.editedOfferId = id;
        console.log(this.editedOfferId);
        this.editingarray = this.offersArray.filter(function (offer) {
            return offer._id == _this.editedOfferId;
        });
        this.editVoucherForm.patchValue({
            titleEnglish: this.editingarray[0].title[1].value,
            titleArabic: this.editingarray[0].title[0].value,
            descriptionEnglish: this.editingarray[0].body[0].value,
            descriptionArabic: this.editingarray[0].body[1].value,
        });
        this.offersArray = this.offersArray.map(function (offer) {
            if (offer._id == id) {
                offer.editingNow = true;
            }
            return offer;
        });
    };
    NewsComponent.prototype.editsumbit = function () {
        var _this = this;
        this.creationDate = new Date(Date.now());
        this.addedOffer = new __WEBPACK_IMPORTED_MODULE_2__news__["a" /* News */](this.editVoucherForm.value.titleEnglish, this.editVoucherForm.value.titleArabic, this.editVoucherForm.value.descriptionEnglish, this.editVoucherForm.value.descriptionArabic, this.creationDate);
        console.log(this.addedOffer);
        this.offerService.editOffer(this.addedOffer, this.editedOfferId);
        this.editVoucherForm.reset();
        setTimeout(function () {
            _this.sumbitdateclicked();
        }, 2000);
        setTimeout(function () {
            _this.sumbitdateclicked();
        }, 2000);
        setTimeout(function () {
            _this.sumbitdateclicked();
        }, 2000);
        setTimeout(function () {
            _this.sumbitdateclicked();
        }, 2000);
        setTimeout(function () {
            _this.sumbitdateclicked();
        }, 2000);
        setTimeout(function () {
            _this.sumbitdateclicked();
        }, 2000);
        this.offerService.addOfferimg(this.editedOfferId, this.files);
    };
    NewsComponent.prototype.delete = function (name) {
        this.offerService.delOffer(name);
    };
    NewsComponent.prototype.hide = function () {
        this.divVisable = false;
    };
    NewsComponent.prototype.pdfHandler = function (event) {
        console.log('pdfhandler called');
        this.files = event.target.files;
        this.offerService.files = event.target.files;
    };
    NewsComponent.prototype.test = function () {
        console.log('sdfdsss');
    };
    return NewsComponent;
}());
NewsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-offer',
        template: __webpack_require__("../../../../../src/app/news/news.component.html"),
        styles: [__webpack_require__("../../../../../src/app/news/news.component.less")],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__news_service__["a" /* NewsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__news_service__["a" /* NewsService */]) === "function" && _a || Object])
], NewsComponent);

var _a;
//# sourceMappingURL=news.component.js.map

/***/ }),

/***/ "../../../../../src/app/news/news.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewsService = (function () {
    function NewsService(http, httpPoster) {
        this.http = http;
        this.httpPoster = httpPoster;
        this.valid = true;
        this.conditions = true;
    }
    NewsService.prototype.getOffers = function (sinceDate) {
        var url = "im4booking/news";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var hh = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        hh.append('Content-Type', 'application/json');
        hh.append('Access-Control-Allow-Origin', '*');
        hh.append('Access-Control-Allow-Headers', '*');
        hh.append('Access-Control-Allow-Methods', '*');
        hh.append('Access-Control-Allow-Credentials', '*');
        hh.append('Access-Control-Allow-Request-Headers', '*');
        hh.append('Access-Control-Allow-Request-Methods', '*');
        hh.append('Access-Control-Origin', '*');
        hh.append('Accept', 'application/json');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: hh
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            return response['_body'];
        });
    };
    NewsService.prototype.getOffer = function (id) {
        var url = "/im4booking/news/" + id;
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var hh = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        hh.append('api_key', api_key);
        hh.append('user_id', user_id);
        hh.append('Content-Type', 'application/json');
        hh.append('Access-Control-Allow-Origin', '*');
        hh.append('Access-Control-Allow-Headers', '*');
        hh.append('Access-Control-Allow-Methods', '*');
        hh.append('Access-Control-Allow-Credentials', '*');
        hh.append('Access-Control-Allow-Request-Headers', '*');
        hh.append('Access-Control-Allow-Request-Methods', '*');
        hh.append('Access-Control-Origin', '*');
        hh.append('Accept', 'application/json');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: hh
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            return response['_body'];
        });
    };
    NewsService.prototype.addOffer = function (offer) {
        var _this = this;
        var url = "/im4booking/news/";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        console.log({ title: [
                {
                    lang: "ar",
                    value: offer.titlear
                },
                {
                    lang: "en",
                    value: offer.title
                }
            ],
            body: [
                {
                    lang: "ar",
                    value: offer.bodyar
                },
                {
                    lang: "en",
                    value: offer.body
                }
            ],
            creation_date: offer.date,
            img_path: "TO BE uploaded",
            media_path: "TO BE uploaded" });
        this.httpPoster.post(url, {
            'user_id': user_id,
            'api_key': api_key,
            'news': { title: [
                    {
                        lang: "ar",
                        value: offer.titlear
                    },
                    {
                        lang: "en",
                        value: offer.title
                    }
                ],
                body: [
                    {
                        lang: "ar",
                        value: offer.bodyar
                    },
                    {
                        lang: "en",
                        value: offer.body
                    }
                ],
                creation_date: offer.date,
                img_path: "TO BE uploaded",
                media_path: "TO BE uploaded"
            }
        }).subscribe(function (data) {
            console.log(data);
            _this.offerbeingCreatedID = data['response'];
            console.log("the response   ", _this.offerbeingCreatedID);
            _this.offerbeingCreatedID = _this.offerbeingCreatedID['_id'];
            console.log("the id   ", _this.offerbeingCreatedID);
            _this.createOfferimg();
            if (!(_this.offerbeingCreatedID)) {
                _this.err = "ERROR";
            }
            else {
                _this.err = "Success";
            }
        });
    };
    NewsService.prototype.addOfferpdf = function (offerId, files) {
        var _this = this;
        var url = "/upload_media/news";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        // header.append('Accept', 'multipart/form-data');
        // header.append('Content-Type', 'multipart/form-data');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header
        });
        var formData = new FormData();
        for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
            var file = files_1[_i];
            formData.append('file', file, file.name);
        }
        formData.append('user_id', user_id);
        formData.append('news_id', offerId);
        formData.append('id', this.offerbeingCreatedID);
        this.http.post(url, formData, options).subscribe(function (data) {
            _this.valid = data['valid'];
            _this.err = data['msg'];
        });
    };
    NewsService.prototype.addOfferimg = function (offerId, files) {
        var _this = this;
        var url = "/upload_pic/news";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        // header.append('Accept', 'multipart/form-data');
        // header.append('Content-Type', 'multipart/form-data');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header
        });
        var formData = new FormData();
        for (var _i = 0, files_2 = files; _i < files_2.length; _i++) {
            var file = files_2[_i];
            formData.append('file', file, file.name);
        }
        formData.append('user_id', user_id);
        formData.append('news_id', offerId);
        formData.append('id', this.offerbeingCreatedID);
        this.http.post(url, formData, options).subscribe(function (data) {
            _this.valid = data['valid'];
            _this.err = data['msg'];
        });
    };
    NewsService.prototype.editOffer = function (offer, offerId) {
        var _this = this;
        var url = "im4booking/news/edit";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        this.httpPoster.post(url, {
            'user_id': user_id,
            'api_key': api_key,
            'news_id': offerId,
            'new_news': { title: [
                    {
                        lang: "ar",
                        value: offer.titlear
                    },
                    {
                        lang: "en",
                        value: offer.title
                    }
                ],
                body: [
                    {
                        lang: "ar",
                        value: offer.bodyar
                    },
                    {
                        lang: "en",
                        value: offer.body
                    }
                ],
                creation_date: offer.date,
                img_path: "TO BE uploaded",
                media_path: "TO BE uploaded"
            }
        }).subscribe(function (data) {
            _this.valid = data.valid;
            _this.err = data.msg;
        });
    };
    NewsService.prototype.createOfferimg = function () {
        console.log(this.offerbeingCreatedID);
        this.addOfferimg(this.offerbeingCreatedID, this.files);
    };
    NewsService.prototype.delOffer = function (offerId) {
        var _this = this;
        var url = "im4booking/news/delete";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        this.httpPoster.post(url, {
            'user_id': user_id,
            'api_key': api_key,
            'news_id': offerId,
        }).subscribe(function (data) {
            _this.valid = data.valid;
            _this.err = data.msg;
        });
    };
    return NewsService;
}());
NewsService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */]) === "function" && _b || Object])
], NewsService);

var _a, _b;
//# sourceMappingURL=news.service.js.map

/***/ }),

/***/ "../../../../../src/app/news/news.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return News; });
var News = (function () {
    function News(title, titlear, body, bodyar, date) {
        this.title = title;
        this.titlear = titlear;
        this.body = body;
        this.bodyar = bodyar;
        this.date = date;
    }
    return News;
}());

//# sourceMappingURL=news.js.map

/***/ }),

/***/ "../../../../../src/app/offer/Offer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Offer; });
var Offer = (function () {
    function Offer(name, namear, description, descriptionar, exp_date, creationDate, img_path, type, condition, conditionar, price) {
        this.name = name;
        this.description = description;
        this.exp_date = exp_date;
        this.creationDate = creationDate;
        this.img_path = img_path;
        this.type = type;
        this.condition = condition;
        this.conditionar = conditionar;
        this.titlear = namear;
        this.desar = descriptionar;
        this.price = price;
    }
    return Offer;
}());

//# sourceMappingURL=Offer.js.map

/***/ }),

/***/ "../../../../../src/app/offer/offer.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>\n<body>\n  <md-tab-group>\n    <md-tab label=\"VIEW OFFERS\">\n        <button class=\"btn btn-primary\" (click)=\"sumbitdateclicked()\">REFRESH</button>\n      <div class=\"cards-tab\">\n        <h1 class=\"view-title\">Available Offers</h1>\n          <label>{{eror}}</label>\n        <md-card class=\"package-card\" *ngFor=\"let offer of offersArray\">\n          <md-card-header>\n          </md-card-header>\n          <md-card-content>\n            <div>\n                <div><img src=\"{{offer.img_path}}\"></div>\n                <label class=\"label-view\">Name</label>\n                <h4 class=\"data-view\">{{offer.name[1].value}} </h4>\n                <h4 class=\"data-view\" dir=\"rtl\" lang=\"ar\">{{offer.name[0].value}}</h4>\n                <label class=\"label-view\">Description</label>\n                <h4 class=\"data-view\">{{offer.description[0].value}}</h4>\n                <h4 class=\"data-view\" dir=\"rtl\" lang=\"ar\">{{offer.description[1].value}}</h4>\n                <label class=\"label-view\">conditions</label>\n                <h4 class=\"data-view\">{{offer.condition[0].value}}</h4>\n                <h4 class=\"data-view\" dir=\"rtl\" lang=\"ar\">{{offer.condition[1].value}}</h4>\n                <h4 class=\"data-view\">{{offer.price}}</h4>\n            </div>\n\n\n              <div class=\"container\" *ngIf=\"offer.editingNow\">\n                <form (ngSubmit)=\"editsumbit()\"  [formGroup]=\"editVoucherForm\">\n                  <div class=\"form-group\">\n                    <label>Title :</label>\n                    <input type=\"text\" class=\"form-control\" id=\"titleng\" formControlName=\"titleEnglish\" value=\"{{offer.name[1].value}}\"  >\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Title Arabic:</label>\n                    <input type=\"text\" class=\"form-control\" id=\"titlearr\" formControlName=\"titleArabic\"  value=\"{{offer.name[0].value}}\">\n                  </div>\n                  <div class=\"form-group\">\n                    <label for=\"pwd\">Description :</label>\n                    <textarea class=\"form-control\" id=\"descriptioneng\" formControlName=\"descriptionEnglish\" value=\"{{offer.description[0].value}}\" ></textarea>\n                  </div>\n                  <div class=\"form-group\">\n                    <label for=\"pwd\">Description Arabic:</label>\n                    <textarea class=\"form-control\" id=\"descriptionarr\" formControlName=\"descriptionArabic\" value=\"{{offer.description[1].value}}\"></textarea>\n                  </div>\n                  <div class=\"form-group\">\n                    <label for=\"pwd\">Condition :</label>\n                    <textarea class=\"form-control\" id=\"conditioneng\" formControlName=\"conditionEnglish\" value=\"{{offer.condition[1].value}}\" ></textarea>\n                  </div>\n                  <div class=\"form-group\">\n                    <label for=\"pwd\">Condition Arabic:</label>\n                    <textarea class=\"form-control\" id=\"conditionarr\" formControlName=\"conditionArabic\" value=\"{{offer.condition[0].value}}\"></textarea>\n                  </div>\n\n                  <div class=\"form-group\">\n                    <label>Expiry Date</label>\n                      <date-picker formControlName=\"newexpiryDate\" ></date-picker>\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Price :</label>\n                    <input type=\"number\" class=\"form-control\" id=\"prices\"  formControlName=\"points\">\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Image :</label>\n                    <input type=\"file\" class=\"form-control\" id=\"img\" (change)=\"pdfHandler($event)\" >\n                  </div>\n                  <button class=\"btn btn-primary\" type=\"submit\">Submit</button>\n\n                </form>\n              </div>\n\n\n\n\n          </md-card-content>\n          <md-card-actions class=\"card-actions\">\n            <button class=\"edit-button\"md-button (click)=\"edit(offer._id)\">EDIT  </button>\n            <button class=\"delete-button\" (click)=\"delete(offer._id)\" md-button>DELETE</button>\n\n            <label>{{eror}}</label>\n          </md-card-actions>\n\n        </md-card>\n\n      </div>\n\n\n    </md-tab>\n\n\n    <md-tab label=\"ADD NEW OFFER\">\n      <h1 class=\"view-title\">Add New Offer</h1>\n      <div class=\"container\">\n        <form  (ngSubmit)=\"createOffer()\" [formGroup]=\"voucherForm\">\n          <div class=\"form-group\">\n            <label>Title :</label>\n            <input type=\"text\" class=\"form-control\" id=\"titleng\" formControlName=\"titleEnglish\">\n          </div>\n          <div class=\"form-group\">\n            <label>Title Arabic:</label>\n            <input type=\"text\" class=\"form-control\" id=\"titlearr\" formControlName=\"titleArabic\">\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pwd\">Description :</label>\n            <textarea class=\"form-control\" id=\"descriptioneng\" formControlName=\"descriptionEnglish\"></textarea>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pwd\">Description Arabic:</label>\n            <textarea class=\"form-control\" id=\"descriptionarr\" formControlName=\"descriptionArabic\"></textarea>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pwd\">Condition :</label>\n            <textarea class=\"form-control\" id=\"conditioneng\" formControlName=\"conditionEnglish\"></textarea>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pwd\">Condition Arabic:</label>\n            <textarea class=\"form-control\" id=\"conditionarr\" formControlName=\"conditionArabic\"></textarea>\n          </div>\n\n          <div class=\"form-group\">\n            <label>Expiry Date</label>\n              <date-picker formControlName=\"expiryDate\"></date-picker>\n          </div>\n          <div class=\"form-group\">\n            <label>Price :</label>\n            <input type=\"number\" class=\"form-control\" id=\"prices\" formControlName=\"points\">\n          </div>\n          <div class=\"form-group\">\n            <label>Image :</label>\n            <input type=\"file\" class=\"form-control\" id=\"img\" (change)=\"pdfHandler($event)\" formControlName=\"img\">\n          </div>\n          <button class=\"btn btn-primary\" type=\"submit\">Submit</button>\n\n        </form>\n\n      </div>\n    </md-tab>\n  </md-tab-group>\n</body>\n"

/***/ }),

/***/ "../../../../../src/app/offer/offer.component.less":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Lato:300,500);", ""]);

// module
exports.push([module.i, "body {\n  font-family: Lato;\n  width: 100%;\n}\n.cards-tab {\n  margin: auto;\n  width: 75%;\n}\n.delete-button {\n  color: white;\n  background-color: #d50000;\n}\n.card-actions {\n  text-align: right;\n}\n.diva {\n  height: 50%;\n  width: 50%;\n}\n.titlo {\n  font-family: Lato;\n  font-size: 30px;\n}\n.view-title {\n  font-size: 40px;\n  font-weight: bolder;\n  text-align: center;\n}\n.card-content {\n  text-align: left;\n  margin: auto;\n}\n.main-field {\n  margin: auto;\n  font-family: Lato;\n  font-size: 16px;\n  font-weight: bold;\n  height: 30px;\n  width: 100%;\n  border: none;\n  border-bottom: 2px solid #3a08e7;\n}\nth {\n  text-align: left;\n  padding-top: .5em;\n  padding-bottom: .5em;\n  width: auto;\n}\ntr {\n  margin: 15px;\n  padding: 15px;\n}\ntd {\n  width: 100%;\n  text-align: justify;\n}\ntable {\n  width: 80%;\n  font-family: Lato;\n  font-size: 16px;\n  margin: auto;\n  text-align: center;\n}\n.form-actions {\n  text-align: right;\n}\n.form-actions button {\n  background-color: #3a08e7;\n  color: white;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/offer/offer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__offer_service__ = __webpack_require__("../../../../../src/app/offer/offer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Offer__ = __webpack_require__("../../../../../src/app/offer/Offer.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng_zorro_antd__ = __webpack_require__("../../../../ng-zorro-antd/esm5/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OfferComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OfferComponent = (function () {
    function OfferComponent(offerService, _message) {
        this.offerService = offerService;
        this._message = _message;
        this.filter = false;
        this.filesb = false;
        this.divVisable = false;
        this.called = false;
        this.visable = false;
    }
    OfferComponent.prototype.ngOnInit = function () {
        this.eror = this.offerService.err;
        this.viewOffers(this.neededDate);
        this.voucherForm = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormGroup"]({
            titleEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            titleArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](''),
            descriptionEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            descriptionArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](''),
            conditionEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](''),
            conditionArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](''),
            package: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            startDate: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            expiryDate: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            oneUse: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            points: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            img: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('')
        });
        this.editVoucherForm = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormGroup"]({
            titleEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            titleArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            descriptionEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            descriptionArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            conditionEnglish: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            conditionArabic: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            package: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            startDate: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            oneUse: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            points: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            newexpiryDate: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
        });
    };
    OfferComponent.prototype.viewOffers = function (date) {
        var _this = this;
        this.offerService.getOffers(date).then(function (res) {
            _this.esponse = JSON.parse(res)['response'];
            if (JSON.parse(res)['valid']) {
                _this.offersArray = _this.esponse;
                _this.offersArray = _this.offersArray.map(function (offer) {
                    if (offer.img_path == "" || offer.img_path == null || offer.img_path == "TO BE uploaded") {
                        offer.img_path = "/offer.png";
                    }
                    offer.editingNow = false;
                    return offer;
                });
            }
            else {
            }
        });
    };
    OfferComponent.prototype.sumbitdateclicked = function () {
        this.neededDate = new Date(Date.now());
        this.viewOffers(this.neededDate);
    };
    OfferComponent.prototype.viewOffer = function (id) {
        var _this = this;
        this.visable = true;
        this.editedOfferId = id;
        console.log(this.editedOfferId);
        this.offerService.getOffer(id).then(function (res) {
            _this.esponse = JSON.parse(res)['response'];
            if (JSON.parse(res)['valid']) {
                _this.viewedOffer = _this.esponse['data'];
            }
            else {
            }
        });
    };
    OfferComponent.prototype.createOffer = function () {
        this.creationDate = new Date(Date.now());
        this.expdate = new Date(this.voucherForm.value.expiryDate._d);
        this.addedOffer = new __WEBPACK_IMPORTED_MODULE_2__Offer__["a" /* Offer */](this.voucherForm.value.titleEnglish, this.voucherForm.value.titleArabic, this.voucherForm.value.descriptionEnglish, this.voucherForm.value.descriptionArabic, this.expdate, this.creationDate, 'img to be ', "offer", this.voucherForm.value.conditionEnglish, this.voucherForm.value.conditionArabic, this.voucherForm.value.points);
        console.log(this.addedOffer);
        console.log("test");
        this.filesb = true;
        this.offerService.addOffer(this.addedOffer);
        this.voucherForm.reset();
        this._message.create('success', 'Offer Added');
    };
    OfferComponent.prototype.edit = function (id) {
        var _this = this;
        this.divVisable = true;
        this.editedOfferId = id;
        this.editingarray = this.offersArray.filter(function (offer) {
            return offer._id == _this.editedOfferId;
        });
        this.offersArray = this.offersArray.map(function (offer) {
            if (offer._id == id) {
                offer.editingNow = true;
            }
            return offer;
        });
        this.editVoucherForm.patchValue({
            titleArabic: this.editingarray[0].name[0].value,
            titleEnglish: this.editingarray[0].name[1].value,
            points: this.editingarray[0].price,
            descriptionEnglish: this.editingarray[0].description[0].value,
            descriptionArabic: this.editingarray[0].description[1].value,
            newexpiryDate: this.editingarray[0].exp_date,
            conditionEnglish: this.editingarray[0].condition[1].value,
            conditionArabic: this.editingarray[0].condition[0].value,
        });
        console.log(this.editingarray[0].description[1].value);
        this.editVoucherForm.value.descriptionEnglish = this.editingarray[0].description[1].value;
        console.log(this.editedOfferId);
    };
    OfferComponent.prototype.editsumbit = function () {
        var _this = this;
        this.creationDate = new Date(Date.now());
        this.expdate = new Date(this.editVoucherForm.value.newexpiryDate._d);
        this.addedOffer = new __WEBPACK_IMPORTED_MODULE_2__Offer__["a" /* Offer */](this.editVoucherForm.value.titleEnglish, this.editVoucherForm.value.titleArabic, this.editVoucherForm.value.descriptionEnglish, this.editVoucherForm.value.descriptionArabic, this.expdate, this.creationDate, 'img to be uploaded ', "offer", this.editVoucherForm.value.conditionEnglish, this.editVoucherForm.value.conditionArabic, this.editVoucherForm.value.points);
        console.log(this.addedOffer);
        this.offerService.editOffer(this.addedOffer, this.editedOfferId);
        this.editVoucherForm.reset();
        setTimeout(function () {
            _this.sumbitdateclicked();
        }, 2000);
        setTimeout(function () {
            _this.sumbitdateclicked();
        }, 2000);
        setTimeout(function () {
            _this.sumbitdateclicked();
        }, 2000);
        this.sumbitdateclicked();
        this.offerService.addOfferimg(this.editedOfferId, this.files);
    };
    OfferComponent.prototype.hide = function () {
        this.divVisable = false;
    };
    OfferComponent.prototype.delete = function (name) {
        console.log(name);
        this.offerService.delOffer(name);
    };
    OfferComponent.prototype.pdfHandler = function (event) {
        console.log('pdfhandler called');
        this.files = event.target.files;
        this.offerService.files = event.target.files;
    };
    OfferComponent.prototype.test = function () {
        console.log('sdfdsss');
    };
    return OfferComponent;
}());
OfferComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-offer',
        template: __webpack_require__("../../../../../src/app/offer/offer.component.html"),
        styles: [__webpack_require__("../../../../../src/app/offer/offer.component.less")],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__offer_service__["a" /* OfferService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__offer_service__["a" /* OfferService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4_ng_zorro_antd__["b" /* NzMessageService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ng_zorro_antd__["b" /* NzMessageService */]) === "function" && _b || Object])
], OfferComponent);

var _a, _b;
//# sourceMappingURL=offer.component.js.map

/***/ }),

/***/ "../../../../../src/app/offer/offer.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OfferService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OfferService = (function () {
    function OfferService(http, httpPoster) {
        this.http = http;
        this.httpPoster = httpPoster;
        this.valid = true;
        this.conditions = true;
        this.finished = false;
    }
    OfferService.prototype.getOffers = function (sinceDate) {
        var url = "im4booking/offer";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var hh = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        hh.append('Content-Type', 'application/json');
        hh.append('Access-Control-Allow-Origin', '*');
        hh.append('Access-Control-Allow-Headers', '*');
        hh.append('Access-Control-Allow-Methods', '*');
        hh.append('Access-Control-Allow-Credentials', '*');
        hh.append('Access-Control-Allow-Request-Headers', '*');
        hh.append('Access-Control-Allow-Request-Methods', '*');
        hh.append('Access-Control-Origin', '*');
        hh.append('Accept', 'application/json');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: hh
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            return response['_body'];
        });
    };
    OfferService.prototype.getOffer = function (id) {
        var url = "/im4booking/offer/" + id;
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var hh = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        hh.append('api_key', api_key);
        hh.append('user_id', user_id);
        hh.append('Content-Type', 'application/json');
        hh.append('Access-Control-Allow-Origin', '*');
        hh.append('Access-Control-Allow-Headers', '*');
        hh.append('Access-Control-Allow-Methods', '*');
        hh.append('Access-Control-Allow-Credentials', '*');
        hh.append('Access-Control-Allow-Request-Headers', '*');
        hh.append('Access-Control-Allow-Request-Methods', '*');
        hh.append('Access-Control-Origin', '*');
        hh.append('Accept', 'application/json');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: hh
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            return response['_body'];
        });
    };
    OfferService.prototype.addOffer = function (offer) {
        var _this = this;
        var url = "/im4booking/offer/";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        if (offer.conditionar == "" && offer.condition == "")
            this.conditions = false;
        console.log({ name: [
                {
                    lang: "ar",
                    value: offer.titlear
                },
                {
                    lang: "en",
                    value: offer.name
                }
            ],
            description: [
                {
                    lang: "ar",
                    value: offer.description
                },
                {
                    lang: "en",
                    value: offer.desar
                }
            ],
            exp_date: offer.exp_date,
            creation_date: offer.creationDate,
            given_points: offer.given_points,
            condition_type: this.conditions,
            condition: [
                {
                    lang: "ar",
                    value: offer.conditionar
                },
                {
                    lang: "en",
                    value: offer.condition
                }
            ],
            is_voucher: false,
            price: offer.price,
        });
        this.httpPoster.post(url, {
            'user_id': user_id,
            'api_key': api_key,
            'offer': {
                name: [
                    {
                        lang: "ar",
                        value: offer.titlear
                    },
                    {
                        lang: "en",
                        value: offer.name
                    }
                ],
                description: [
                    {
                        lang: "ar",
                        value: offer.description
                    },
                    {
                        lang: "en",
                        value: offer.desar
                    }
                ],
                exp_date: offer.exp_date,
                creation_date: offer.creationDate,
                condition_type: this.conditions,
                condition: [
                    {
                        lang: "ar",
                        value: offer.conditionar
                    },
                    {
                        lang: "en",
                        value: offer.condition
                    }
                ],
                is_voucher: false,
                price: offer.price,
            }
        }).subscribe(function (data) {
            console.log(data);
            _this.offerbeingCreatedID = data['response'];
            console.log("the response   ", _this.offerbeingCreatedID);
            _this.offerbeingCreatedID = _this.offerbeingCreatedID['_id'];
            console.log("the id   ", _this.offerbeingCreatedID);
            if (!(_this.offerbeingCreatedID)) {
                _this.err = "ERROR";
            }
            else {
                _this.err = "Success";
            }
            _this.createOfferimg();
        });
    };
    OfferService.prototype.addOfferpdf = function (offerId, files) {
        var _this = this;
        var url = "/upload_media/offer";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        // header.append('Accept', 'multipart/form-data');
        // header.append('Content-Type', 'multipart/form-data');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header
        });
        var formData = new FormData();
        for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
            var file = files_1[_i];
            formData.append('file', file, file.name);
        }
        formData.append('user_id', user_id);
        formData.append('offer_id', offerId);
        formData.append('id', this.offerbeingCreatedID);
        this.http.post(url, formData, options).subscribe(function (data) {
            _this.valid = data['valid'];
            _this.err = data['msg'];
        });
    };
    OfferService.prototype.addOfferimg = function (offerId, files) {
        var _this = this;
        var url = "/upload_pic/offer";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        // header.append('Accept', 'multipart/form-data');
        // header.append('Content-Type', 'multipart/form-data');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header
        });
        var formData = new FormData();
        for (var _i = 0, files_2 = files; _i < files_2.length; _i++) {
            var file = files_2[_i];
            formData.append('file', file, file.name);
        }
        formData.append('user_id', user_id);
        formData.append('id', offerId);
        this.http.post(url, formData, options).subscribe(function (data) {
            _this.valid = data['valid'];
            _this.err = data['msg'];
        });
    };
    OfferService.prototype.editOffer = function (offer, offerId) {
        var _this = this;
        var url = "/im4booking/offer/edit";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        console.log(offerId, { name: [
                {
                    lang: "ar",
                    value: offer.titlear
                },
                {
                    lang: "en",
                    value: offer.name
                }
            ],
            description: [
                {
                    lang: "ar",
                    value: offer.description
                },
                {
                    lang: "en",
                    value: offer.desar
                }
            ],
            exp_date: offer.exp_date,
            creation_date: offer.creationDate,
            condition_type: this.conditions,
            condition: [
                {
                    lang: "ar",
                    value: offer.conditionar
                },
                {
                    lang: "en",
                    value: offer.condition
                }
            ],
            is_voucher: false,
            price: offer.price,
        });
        this.httpPoster.post(url, {
            'user_id': user_id,
            'api_key': api_key,
            'offer_id': offerId,
            'new_offer': {
                name: [
                    {
                        lang: "ar",
                        value: offer.titlear
                    },
                    {
                        lang: "en",
                        value: offer.name
                    }
                ],
                description: [
                    {
                        lang: "ar",
                        value: offer.description
                    },
                    {
                        lang: "en",
                        value: offer.desar
                    }
                ],
                exp_date: offer.exp_date,
                creation_date: offer.creationDate,
                condition_type: this.conditions,
                condition: [
                    {
                        lang: "ar",
                        value: offer.conditionar
                    },
                    {
                        lang: "en",
                        value: offer.condition
                    }
                ],
                is_voucher: false,
                price: offer.price,
            }
        }).subscribe(function (data) {
            console.log(data);
            console.log(data['msg']);
            console.log(data.msg);
            _this.err = data['msg'];
        });
    };
    OfferService.prototype.createOfferimg = function () {
        console.log(this.offerbeingCreatedID);
        this.addOfferimg(this.offerbeingCreatedID, this.files);
    };
    OfferService.prototype.delOffer = function (offerId) {
        var _this = this;
        var url = "/im4booking/offer/delete";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        this.httpPoster.post(url, {
            'user_id': user_id,
            'api_key': api_key,
            'offer_id': offerId,
        }).subscribe(function (data) {
            console.log(data);
            console.log(data['msg']);
            console.log(data.msg);
            _this.err = data['msg'];
        });
    };
    return OfferService;
}());
OfferService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */]) === "function" && _b || Object])
], OfferService);

var _a, _b;
//# sourceMappingURL=offer.service.js.map

/***/ }),

/***/ "../../../../../src/app/packages/packages.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Lato:300,500);", ""]);

// module
exports.push([module.i, "body{\n  font-family: Lato;\n  width: 100%;\n}\n\n.cards-tab{\n  margin: auto;\n  width: 75%;\n}\n\n.delete-button{\n  color: white;\n  background-color: #d50000;\n}\n\n.card-actions{\n  text-align: right;\n}\n\n.view-title{\n  font-size: 40px;\n  font-weight: bolder;\n  text-align: center;\n}\n\n.card-content{\n  text-align: left;\n  margin: auto;\n}\n\n.main-field{\n  margin: auto;\n  font-family: Lato;\n  font-size: 16px;\n  font-weight: bold;\n  height: 30px;\n  width: 100%;\n  border: none;\n  border-bottom: 2px solid #3a08e7;\n}\n\nth {\n  text-align: left;\n  padding-top: .5em;\n  padding-bottom: .5em;\n  width: auto;\n}\n\ntr {\n  margin: 15px;\n  padding: 15px;\n}\n\ntd {\n  width: 100%;\n  text-align: justify;\n}\n\n table {\n   width: 80%;\n   font-family: Lato;\n   font-size: 16px;\n   margin: auto;\n   text-align: center;\n }\n\n.form-actions {\n  text-align: right;\n}\n\n.form-actions button {\n  background-color: #3a08e7;\n  color: white;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/packages/packages.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>\n<body>\n  <md-tab-group [selectedIndex]=\"selectedTab\">\n    <md-tab label=\"View Packages\">\n      <div class=\"cards-tab\">\n        <button class=\"refresh-button\" md-button (click)=\"refresh()\">Refresh</button>\n        <h1 class=\"view-title\">Available Packages</h1>\n        <md-card class=\"package-card\" *ngFor=\"let pack of packagesArr\">\n          <md-card-header>\n          </md-card-header>\n          <md-card-content>\n            <div>\n                <div><img src=\"{{pack.img_path}}\"></div>\n                <label class=\"label-view\">Name:</label>\n                <h4 class=\"data-view\">{{pack.name[0].value}} </h4>\n                <h4 class=\"data-view\" dir=\"rtl\" lang=\"ar\">{{pack.name[1].value}}</h4>\n                <label class=\"label-view\">Description:</label>\n                <h4 class=\"data-view\">{{pack.description[0].value}}</h4>\n                <h4 class=\"data-view\" dir=\"rtl\" lang=\"ar\">{{pack.description[1].value}}</h4>\n                <div>\n                  <label class=\"label-view\">Description:</label>\n                  <h4 class=\"data-view\">{{pack.description[0].value}}</h4>\n                  <h4 class=\"data-view\" dir=\"rtl\" lang=\"ar\">{{pack.description[1].value}}</h4>\n                </div>\n            </div>\n            <!-- <img src=\"{{pack.imgurl}}\" /> -->\n            <div class=\"container\" *ngIf=\"pack.edit_enabled\">\n              <form (ngSubmit)=\"editPackageSubmit()\" [formGroup]=\"editPackageForm\">\n                <div class=\"form-group\">\n                  <label>Package Name:</label>\n                  <input type=\"text\" class=\"form-control\" formControlName=\"nameEnglish\" value=\"{{pack.name[0].value}}\">\n                </div>\n                <div class=\"form-group\">\n                  <label>Package Name in Arabic:</label>\n                  <input type=\"text\" class=\"form-control\" formControlName=\"nameArabic\" value=\"{{pack.name[1].value}}\">\n                </div>\n                <div class=\"form-group\">\n                  <label for=\"pwd\">Points:</label>\n                  <input type=\"number\" class=\"form-control\" formControlName=\"points\" value=\"{{pack.points}}\">\n                </div>\n                <div class=\"form-group\">\n                  <label>Description :</label>\n                  <textarea class=\"form-control\" id=\"description\" formControlName=\"descriptionEnglish\" value=\"{{pack.description[0].value}}\"></textarea>\n                </div>\n                <div class=\"form-group\">\n                  <label>Description Arabic:</label>\n                  <textarea class=\"form-control\" id=\"descriptionAr\" formControlName=\"descriptionArabic\" value=\"{{pack.description[1].value}}\"></textarea>\n                </div>\n                <div class=\"form-group\">\n                  <label>Image</label>\n                  <input type=\"file\" class=\"form-control\" (change)=\"editFileHandler(1,$event)\">\n                </div>\n                <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n              </form>\n            </div>\n          </md-card-content>\n          <md-card-actions class=\"card-actions\">\n            <button class=\"edit-button\" md-button (click)=\"editPackage(pack._id)\">EDIT</button>\n            <button class=\"edit-button\" md-button (click)=\"packageStatusChanger(pack.activate, pack._id)\">{{pack.status}}</button>\n          </md-card-actions>\n          <br>\n          <br>\n        </md-card>\n      </div>\n    </md-tab>\n    <md-tab label=\"Add New Package\">\n    <div class='container'>\n      <form (ngSubmit)=\"newPackageSubmit()\" [formGroup]=\"packageForm\">\n        <div class=\"form-group\">\n          <label>Package Name:</label>\n          <input type=\"text\" class=\"form-control\" formControlName=\"nameEnglish\">\n        </div>\n        <div class=\"form-group\">\n          <label>Package Name in Arabic:</label>\n          <input type=\"text\" class=\"form-control\" formControlName=\"nameArabic\">\n        </div>\n        <div class=\"form-group\">\n          <label for=\"pwd\">Points:</label>\n          <input type=\"number\" class=\"form-control\" formControlName=\"points\">\n        </div>\n        <div class=\"form-group\">\n          <label>Description :</label>\n          <textarea class=\"form-control\" id=\"description\" formControlName=\"descriptionEnglish\"></textarea>\n        </div>\n        <div class=\"form-group\">\n          <label>Description Arabic:</label>\n          <textarea class=\"form-control\" id=\"descriptionAr\" formControlName=\"descriptionArabic\"></textarea>\n        </div>\n        <!-- <div class=\"form-group\">\n          <div *ngFor=\"let packVoucher of packVoucherArr\">\n            <label>{{packVoucher.title}}</label>\n            <input type=\"checkbox\" class=\"form-control\" (selected)=\"addPackVoucher(packVoucher.id)\">\n          </div>\n        </div> -->\n        <div class=\"form-group\">\n          <label>Image</label>\n          <input type=\"file\" class=\"form-control\" (change)=\"fileHandler(1,$event)\">\n        </div>\n        <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n      </form>\n      <h4>{{statusMessage}}</h4>\n    </div>\n    </md-tab>\n    <md-tab label=\"View Vouchers\">\n      <div class=\"cards-tab\">\n        <button class=\"refresh-button\" md-button (click)=\"refresh()\">Refresh</button>\n        <h1 class=\"view-title\">Available Vouchers</h1>\n        <div>\n            <label>Diamond</label>\n            <input type=\"checkbox\" [(ngModel)]=\"filterDiamond\">\n            <label>Platineum</label>\n            <input type=\"checkbox\" [(ngModel)]=\"filterPlat\">\n            <label>Silver</label>\n            <input type=\"checkbox\" [(ngModel)]=\"filterSilv\">\n            <label>Bronze</label>\n            <input type=\"checkbox\" [(ngModel)]=\"filterBronze\">\n            <label>Gold</label>\n            <input type=\"checkbox\" [(ngModel)]=\"filterGold\">\n            <button class=\"btn btn-primary\" (click)=\"filter()\">Filter</button>\n          </div>\n        <div *ngFor=\"let list of vvList\">\n          <h2>{{list.name}}</h2>\n          <br>\n          <md-card class=\"package-card\" *ngFor=\"let voucher of list.vouchers\">\n          <md-card-header>\n          </md-card-header>\n          <md-card-content>\n              <div>\n                  <div><img src=\"{{voucher.img_path}}\"></div>\n                  <label class=\"label-view\">Name:</label>\n                  <h4 class=\"data-view\">{{voucher.name[0].value}}</h4>\n                  <h4 class=\"data-view\" dir=\"rtl\" lang=\"ar\">{{voucher.name[1].value}}</h4>\n                  <label class=\"label-view\">Description:</label>\n                  <h4 class=\"data-view\">{{voucher.description[0].value}}</h4>\n                  <h4 class=\"data-view\" dir=\"rtl\" lang=\"ar\">{{voucher.description[1].value}}</h4>\n              </div>\n           <div *ngIf=\"voucher.edit_enabled\">\n            <form (ngSubmit)=\"editVoucherSubmit()\" [formGroup]=\"editVoucherForm\">\n              <div class=\"form-group\">\n                <label>Title :</label>\n                <input type=\"text\" class=\"form-control\" id=\"title\" formControlName=\"titleEnglish\" value=\"{{voucher.name[0].value}}\">\n              </div>\n              <div class=\"form-group\">\n                <label>Title Arabic:</label>\n                <input type=\"text\" class=\"form-control\" id=\"title\" formControlName=\"titleArabic\" value=\"{{voucher.name[1].value}}\">\n              </div>\n              <div class=\"form-group\">\n                <label for=\"pwd\">Description :</label>\n                <textarea class=\"form-control\" id=\"description\" formControlName=\"descriptionEnglish\" value=\"{{voucher.description[0].value}}\"></textarea>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"pwd\">Description Arabic:</label>\n                <textarea class=\"form-control\" id=\"description\" formControlName=\"descriptionArabic\" value=\"{{voucher.description[1].value}}\"></textarea>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"pwd\">Condition :</label>\n                <textarea class=\"form-control\" id=\"condition\" formControlName=\"conditionEnglish\" value=\"{{voucher.condition[0].value}}\"></textarea>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"pwd\">Condition Arabic:</label>\n                <textarea class=\"form-control\" id=\"condition\" formControlName=\"conditionArabic\" value=\"{{voucher.condition[1].value}}\"></textarea>\n              </div>\n              <div class=\"form-group\">\n                <select class=\"selectpicker\" formControlName=\"package\">\n                  <option value=\"\">Select an Package</option>\n                  <option value=\"1213EEe1\">SeRRe</option>\n                  <option *ngFor=\"let package of packagesArr\" value=\"{{package._id}}\">{{package.name[0].value}}</option>\n                </select>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"pwd\">Only 1 Use : </label>\n                <select class=\"selectpicker\" formControlName=\"oneUse\">\n                  <option value=\"\">Choose Yes or No</option>\n                  <option value=\"true\">Yes</option>\n                  <option value=\"false\">No</option>\n                </select>\n              </div>\n              <div class=\"form-group\">\n                <label>Points :</label>\n                <input type=\"number\" class=\"form-control\" id=\"points\" formControlName=\"points\" value=\"{{voucher.given_points}}\">\n              </div>\n              <div class=\"form-group\">\n                <label>Image :</label>\n                <input type=\"file\" class=\"form-control\" id=\"img\" (change)=\"editFileHandler(0, $event)\">\n              </div>\n              <button class=\"btn btn-primary\" type=\"submit\">Submit</button>\n            </form>\n           </div>\n          </md-card-content>\n          <md-card-actions class=\"card-actions\">\n            <button md-button class=\"edit-button\" (click)=\"editVoucher(voucher._id)\">EDIT</button>\n            <button class=\"delete-button\" (click)=\"deleteVoucher(voucher._id)\" md-button>DELETE</button>\n          </md-card-actions>\n          <br>\n          <br>\n        </md-card>\n        <br>\n        <br>\n        </div>\n      </div>\n    </md-tab>\n    <md-tab label=\"Add New Voucher\">\n      <h1 class=\"view-title\">Add New Voucher</h1>\n      <div class=\"container\">\n        <form (ngSubmit)=\"newVoucherSubmit()\" [formGroup]=\"voucherForm\">\n          <div class=\"form-group\">\n            <label>Title :</label>\n            <input type=\"text\" class=\"form-control\" id=\"title\" formControlName=\"titleEnglish\">\n          </div>\n          <div class=\"form-group\">\n            <label>Title Arabic:</label>\n            <input type=\"text\" class=\"form-control\" id=\"title\" formControlName=\"titleArabic\">\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pwd\">Description :</label>\n            <textarea class=\"form-control\" id=\"description\" formControlName=\"descriptionEnglish\"></textarea>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pwd\">Description Arabic:</label>\n            <textarea class=\"form-control\" id=\"description\" formControlName=\"descriptionArabic\"></textarea>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pwd\">Condition :</label>\n            <textarea class=\"form-control\" id=\"condition\" formControlName=\"conditionEnglish\"></textarea>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pwd\">Condition Arabic:</label>\n            <textarea class=\"form-control\" id=\"condition\" formControlName=\"conditionArabic\"></textarea>\n          </div>\n          <div class=\"form-group\">\n            <select class=\"selectpicker\" formControlName=\"package\">\n              <option value=\"\">Select an Package</option>\n              <option *ngFor=\"let package of packagesArr\" value=\"{{package._id}}\">{{package.name[0].value}}</option>\n            </select>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pwd\">Only 1 Use : </label>\n            <select class=\"selectpicker\" formControlName=\"oneUse\">\n              <option value=\"\">Choose Yes or No</option>\n              <option value=\"yes\">Yes</option>\n              <option value=\"no\">No</option>\n            </select>\n          </div>\n          <div class=\"form-group\">\n            <label>Points :</label>\n            <input type=\"number\" class=\"form-control\" id=\"points\" formControlName=\"points\">\n          </div>\n          <div class=\"form-group\">\n            <label>Image :</label>\n            <input type=\"file\" class=\"form-control\" id=\"img\" (change)=\"fileHandler(0, $event)\">\n          </div>\n          <button class=\"btn btn-primary\" type=\"submit\">Submit</button>\n          <div>\n            <h4>{{statusMessage}}</h4>\n          </div>\n        </form>\n      </div>\n    </md-tab>\n  </md-tab-group>\n</body>\n"

/***/ }),

/***/ "../../../../../src/app/packages/packages.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__packages_service__ = __webpack_require__("../../../../../src/app/packages/packages.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng_zorro_antd__ = __webpack_require__("../../../../ng-zorro-antd/esm5/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PackagesComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PackagesComponent = (function () {
    function PackagesComponent(packageService, _message) {
        this.packageService = packageService;
        this._message = _message;
        this.editEnabled = false;
        this.showImageUpload = false;
        this.statusMessage = " ";
        this.selectedTab = 0;
    }
    PackagesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.packageForm = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormGroup"]({
            nameEnglish: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required),
            nameArabic: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            points: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required),
            descriptionEnglish: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required),
            descriptionArabic: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]()
        });
        this.voucherForm = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormGroup"]({
            titleEnglish: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required),
            titleArabic: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            descriptionEnglish: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required),
            descriptionArabic: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            conditionEnglish: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            conditionArabic: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            package: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required),
            oneUse: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required),
            points: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required)
        });
        this.editPackageForm = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormGroup"]({
            nameEnglish: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            nameArabic: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            points: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            descriptionEnglish: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            descriptionArabic: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]()
        });
        this.editVoucherForm = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormGroup"]({
            titleEnglish: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            titleArabic: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            descriptionEnglish: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            descriptionArabic: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            conditionEnglish: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            conditionArabic: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            package: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            oneUse: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"](),
            points: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]()
        });
        this.packageService.getPackages().then(function (res) {
            console.log(res);
            var body = JSON.parse(res);
            _this.packagesArr = body['response'];
            _this.packagesArr = _this.packagesArr.map(function (pack) {
                if (pack.img_path == "" || pack.img_path == null) {
                    pack.img_path = "/pack.jpg";
                }
                pack.edit_enabled = false;
                pack.status = pack.activate ? "Deactivate" : "Activate";
                console.log("STATUS PAC", pack);
                return pack;
            });
            console.log(_this.packagesArr);
        });
        this.packageService.getVouchers().then(function (res) {
            var body = JSON.parse(res);
            console.log(body);
            var list = body['response'];
            _this.voucherList = list;
            _this.vvList = _this.voucherList['list'];
            console.log("EBN EL GAZMA", _this.voucherList);
            console.log("EBN EL GAZMA 2", _this.vvList);
            _this.vouchersArr = Array();
            _this.vvList.forEach(function (e) {
                var ee = e.vouchers;
                var vv_enhanced = ee.map(function (voucher) {
                    if (voucher['img_path'] == "" || voucher['img_path'] == null) {
                        voucher['img_path'] = "/voucher.png";
                    }
                    voucher['edit_enabled'] = false;
                    voucher.type = e.name;
                    return voucher;
                });
                vv_enhanced.forEach(function (vvv) {
                    _this.vouchersArr.push(vvv);
                });
            });
            _this.vvListFilter = _this.vvList;
        });
    };
    PackagesComponent.prototype.filter = function () {
        var _this = this;
        this.vvList = this.vvListFilter.filter(function (val) {
            console.log(_this.filterPlat);
            if (_this.filterBronze) {
                return val.name === 'Bronze Membership';
            }
            else if (_this.filterPlat) {
                return val.name === 'PLATINUM MEMBERSHIP';
            }
            else if (_this.filterDiamond) {
                return val.name === 'DIAMOND MEMBERSHIP';
            }
            else if (_this.filterSilv) {
                return val.name === 'SILVER MEMBERSHIP';
            }
            else if (_this.filterGold) {
                return val.name === 'GOLD MEMBERSHIP';
            }
            else {
                return true;
            }
        });
    };
    PackagesComponent.prototype.newPackageSubmit = function () {
        var _this = this;
        if (this.packageForm.valid) {
            var data = {
                'name': [
                    {
                        'lang': 'en',
                        'value': this.packageForm.value.nameEnglish
                    },
                    {
                        'lang': 'ar',
                        'value': this.packageForm.value.nameArabic
                    }
                ],
                'points': this.packageForm.value.points,
                'description': [
                    {
                        'lang': 'en',
                        'value': this.packageForm.value.descriptionEnglish
                    },
                    {
                        'lang': 'ar',
                        'value': this.packageForm.value.descriptionArabic
                    }
                ]
                //'vouchers': []
            };
            console.log(data);
            this.packageService.addPackage(data).then(function (res) {
                var valid = res['valid'];
                console.log("AFTER PAC" + valid);
                if (valid) {
                    var response = res['response'];
                    _this.packageID = response['_id'];
                    _this.packageService.sendFile(_this.packageID, _this.imgFile);
                    _this.statusMessage = "Success";
                    _this._message.create('success', 'Package Added');
                    _this.packageForm.reset();
                    _this.refresh();
                }
                else {
                    _this.statusMessage = "" + res['msg'];
                    _this._message.create('error', 'Error!');
                }
            });
        }
        else {
            this.statusMessage = "Couldn't connect to the internet";
        }
    };
    PackagesComponent.prototype.editPackageSubmit = function () {
        var _this = this;
        if (this.editPackageForm.valid) {
            var data = {
                'name': [
                    {
                        'lang': 'en',
                        'value': this.editPackageForm.value.nameEnglish
                    },
                    {
                        'lang': 'ar',
                        'value': this.editPackageForm.value.nameArabic
                    }
                ],
                'points': this.editPackageForm.value.points,
                'description': [
                    {
                        'lang': 'en',
                        'value': this.editPackageForm.value.descriptionEnglish
                    },
                    {
                        'lang': 'ar',
                        'value': this.editPackageForm.value.descriptionArabic
                    }
                ]
                //'vouchers': []
            };
            console.log(data);
            this.packageService.editPackage(data, this.editedVoucher).then(function (res) {
                var valid = res['valid'];
                console.log("PACK VALID " + valid);
                if (valid) {
                    var response = res['response'];
                    _this.packageID = response['_id'];
                    _this.statusMessage = "Success";
                    _this._message.create('success', 'Package Edited');
                    _this.refresh();
                    if (_this.imgFile.length != null || typeof _this.imgFile.length != undefined) {
                        _this.packageService.sendFile(_this.packageID, _this.imgFile);
                    }
                    //this.packageForm.reset();
                }
                else {
                    _this.statusMessage = "" + res['msg'];
                    _this._message.create('error', 'Package Not Edited');
                }
            });
        }
        else {
            this.statusMessage = "Couldn't connect to the internet";
        }
    };
    PackagesComponent.prototype.editPackage = function (id) {
        var _this = this;
        this.editEnabled = true;
        this.editedVoucher = id;
        this.packagesArr = this.packagesArr.map(function (pack) {
            if (pack._id == _this.editedVoucher) {
                pack.edit_enabled = true;
            }
            return pack;
        });
        var selectedPackage = this.packagesArr.filter(function (voucher) {
            if (voucher._id == _this.editedVoucher) {
                return voucher;
            }
        });
        this.editFormPackage = selectedPackage[0];
        this.editPackageForm.patchValue({
            nameEnglish: this.editFormPackage.name[0].value,
            nameArabic: this.editFormPackage.name[1].value,
            points: this.editFormPackage.points,
            descriptionEnglish: this.editFormPackage.description[0].value,
            descriptionArabic: this.editFormPackage.description[1].value
        });
    };
    PackagesComponent.prototype.cancelEdit = function () {
        this.editEnabled = false;
    };
    PackagesComponent.prototype.submitEditPackage = function () {
    };
    // submitEditVoucher(title: string, desc: string, expdate: string, points: number, condition: string, id: number, package_id: number): void {
    //   const tempVoucher = new Voucher(title, id, desc, expdate, points, condition, package_id);
    //   this.packageService.editVoucher(tempVoucher);
    // }
    PackagesComponent.prototype.editVoucher = function (voucherId) {
        var _this = this;
        this.editEnabled = true;
        this.editedVoucher = voucherId;
        this.vouchersArr = this.vouchersArr.map(function (voucher) {
            if (_this.editedVoucher == voucher['_id']) {
                voucher['edit_enabled'] = true;
            }
            return voucher;
        });
        var selectedPackage = this.vouchersArr.filter(function (voucher) {
            if (voucher['_id'] == _this.editedVoucher) {
                return voucher;
            }
        });
        this.editFormVoucher = selectedPackage[0];
        // console.log("CONDITION" + this.editFormVoucher.condition[0].value);
        this.editVoucherForm.patchValue({
            titleEnglish: this.editFormVoucher['name'][0]['value'],
            titleArabic: this.editFormVoucher['name'][1]['value'],
            descriptionEnglish: this.editFormVoucher.description[0].value,
            descriptionArabic: this.editFormVoucher.description[1].value,
            conditionEnglish: this.editFormVoucher.condition[0].value,
            conditionArabic: this.editFormVoucher.condition[1].value,
            package: this.editFormVoucher.in_package,
            oneUse: this.editFormVoucher.used_one,
            points: this.editFormVoucher.given_points
        });
    };
    PackagesComponent.prototype.fileHandler = function (type, event) {
        var objId;
        this.imgFile = event.target.files;
        // if (type == 1) {
        //   objId = this.packageID;
        //   console.log(`sending file for ${objId}`);
        //   //this.packageService.sendFile(objId, this.imgFile);
        // } else if (type == 0) {
        //   objId = this.voucherID;
        //   console.log(`sending file for ${objId}`);
        //   this.packageService.sendFileImgVoucher(objId, this.imgFile).then(res => {
        //   });
        // } else {
        //   objId = this.voucherID;
        //   console.log(`sending file for ${objId}`);
        //   this.packageService.sendFilePDFVoucher(objId, this.imgFile);
        // }
    };
    PackagesComponent.prototype.editFileHandler = function (type, event) {
        var objId;
        this.imgFile = event.target.files;
        if (type == 1) {
            objId = this.packageID;
            console.log("sending file for " + objId);
            this.packageService.sendFile(objId, this.imgFile);
        }
        else if (type == 0) {
            objId = this.voucherID;
            console.log("sending file for " + objId);
            this.packageService.sendFileImgVoucher(objId, this.imgFile).then(function (res) {
            });
        }
        else {
            objId = this.voucherID;
            console.log("sending file for " + objId);
            this.packageService.sendFilePDFVoucher(objId, this.imgFile);
        }
    };
    PackagesComponent.prototype.addPackVoucher = function (id) {
        this.packVoucherArr.push(id.toString());
    };
    PackagesComponent.prototype.newVoucherSubmit = function () {
        var _this = this;
        if (this.voucherForm.valid) {
            var ob;
            if (this.voucherForm.value.oneUse == "true") {
                ob = true;
            }
            else {
                ob = false;
            }
            var data = {
                'name': [
                    {
                        'lang': 'en',
                        'value': this.voucherForm.value.titleEnglish
                    },
                    {
                        'lang': 'ar',
                        'value': this.voucherForm.value.titleArabic
                    }
                ],
                'description': [
                    {
                        'lang': 'en',
                        'value': this.voucherForm.value.descriptionEnglish
                    },
                    {
                        'lang': 'ar',
                        'value': this.voucherForm.value.descriptionArabic
                    }
                ],
                'is_voucher': true,
                'given_points': this.voucherForm.value.points,
                'condition': [
                    {
                        'lang': 'en',
                        'value': this.voucherForm.value.conditionEnglish
                    },
                    {
                        'lang': 'ar',
                        'value': this.voucherForm.value.conditionArabic
                    }
                ],
                'condition_type': true,
                'used_one': ob,
                'in_package': this.voucherForm.value.package
            };
            console.log(data);
            this.packageService.addVouchers(data, this.voucherForm.value.package).then(function (res) {
                var valid = res['valid'];
                if (valid == true) {
                    var body = res['response'];
                    _this.voucherID = body['_id'];
                    _this.packageService.sendFileImgVoucher(_this.voucherID, _this.imgFile);
                    _this.statusMessage = "Voucher Added";
                    _this._message.create('success', 'Voucher Added');
                    _this.voucherForm.reset();
                    _this.refresh();
                }
                else {
                    _this.statusMessage = "" + res['msg'];
                    _this._message.create('error', 'Voucher Not Added');
                }
            });
        }
        else {
            console.log(this.voucherForm.value);
        }
    };
    PackagesComponent.prototype.editVoucherSubmit = function () {
        var _this = this;
        if (this.editVoucherForm.valid) {
            var ob;
            if (this.editVoucherForm.value.oneUse == "true") {
                ob = true;
            }
            else {
                ob = false;
            }
            var data = {
                'name': [
                    {
                        'lang': 'en',
                        'value': this.editVoucherForm.value.titleEnglish
                    },
                    {
                        'lang': 'ar',
                        'value': this.editVoucherForm.value.titleArabic
                    }
                ],
                'description': [
                    {
                        'lang': 'en',
                        'value': this.editVoucherForm.value.descriptionEnglish
                    },
                    {
                        'lang': 'ar',
                        'value': this.editVoucherForm.value.descriptionArabic
                    }
                ],
                'is_voucher': true,
                'given_points': this.editVoucherForm.value.points,
                'condition': [
                    {
                        'lang': 'en',
                        'value': this.editVoucherForm.value.conditionEnglish
                    },
                    {
                        'lang': 'ar',
                        'value': this.editVoucherForm.value.conditionArabic
                    }
                ],
                'condition_type': true,
                'used_one': ob,
                'in_package': this.editVoucherForm.value.package
            };
            console.log(data);
            this.packageService.editVoucher(data, this.editVoucherForm.value.package, this.editedVoucher).then(function (res) {
                console.log("EDIT RES" + res);
                var valid = res['valid'];
                if (valid == true) {
                    var body = res['response'];
                    _this.voucherID = body['_id'];
                    _this.refresh();
                    _this._message.create('success', 'Voucher Edited');
                    _this.packageService.sendFileImgVoucher(_this.editedVoucher, _this.imgFile);
                    //this.voucherForm.reset();
                }
                else {
                    _this._message.create('error', 'Voucher Not Edited');
                }
            });
        }
        else {
            console.log(this.voucherForm.value);
        }
    };
    PackagesComponent.prototype.deletePackage = function (id) {
        var _this = this;
        this.packageService.deletePackage(id).then(function (res) {
            console.log("DELETE RES" + res);
            _this.refresh();
        });
    };
    PackagesComponent.prototype.deleteVoucher = function (id) {
        var _this = this;
        this.packageService.deleteVoucher(id).then(function (res) {
            console.log("DELETE RES" + res);
            _this.refresh();
        });
    };
    PackagesComponent.prototype.refresh = function () {
        var _this = this;
        this.packageService.getPackages().then(function (res) {
            console.log(res);
            var body = JSON.parse(res);
            _this.packagesArr = body['response'];
            _this.packagesArr = _this.packagesArr.map(function (pack) {
                if (pack.img_path == "" || pack.img_path == null) {
                    pack.img_path = "/pack.jpg";
                }
                pack.edit_enabled = false;
                pack.status = pack.activate ? "Deactivate" : "Activate";
                console.log("STATUS PAC", pack);
                return pack;
            });
            console.log(_this.packagesArr);
        });
        this.packageService.getVouchers().then(function (res) {
            var body = JSON.parse(res);
            console.log(body);
            var list = body['response'];
            _this.voucherList = list;
            _this.vvList = _this.voucherList['list'];
            console.log("EBN EL GAZMA", _this.voucherList);
            console.log("EBN EL GAZMA 2", _this.vvList);
            _this.vouchersArr = Array();
            _this.vvList.forEach(function (e) {
                var ee = e.vouchers;
                var vv_enhanced = ee.map(function (voucher) {
                    if (voucher['img_path'] == "" || voucher['img_path'] == null) {
                        voucher['img_path'] = "/voucher.png";
                    }
                    voucher['edit_enabled'] = false;
                    return voucher;
                });
                vv_enhanced.forEach(function (vvv) {
                    _this.vouchersArr.push(vvv);
                });
            });
        });
    };
    PackagesComponent.prototype.deactivatePackage = function (id) {
        this.packageService.deactivatePackage(id);
    };
    PackagesComponent.prototype.activatePackage = function (id) {
        this.packageService.activatePackage(id);
    };
    PackagesComponent.prototype.packageStatusChanger = function (status, id) {
        if (status) {
            this.deactivatePackage(id);
        }
        else {
            this.activatePackage(id);
        }
        this.refresh();
    };
    return PackagesComponent;
}());
PackagesComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-packages',
        template: __webpack_require__("../../../../../src/app/packages/packages.component.html"),
        styles: [__webpack_require__("../../../../../src/app/packages/packages.component.css")],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__packages_service__["a" /* PackagesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__packages_service__["a" /* PackagesService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3_ng_zorro_antd__["b" /* NzMessageService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ng_zorro_antd__["b" /* NzMessageService */]) === "function" && _b || Object])
], PackagesComponent);

var _a, _b;
//# sourceMappingURL=packages.component.js.map

/***/ }),

/***/ "../../../../../src/app/packages/packages.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PackagesService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PackagesService = (function () {
    function PackagesService(http) {
        this.http = http;
    }
    PackagesService.prototype.addPackage = function (data) {
        var url = "im4booking/package";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var body = {
            'api_key': api_key,
            'user_id': user_id,
            'package': data
        };
        return this.http.post(url, body).toPromise().then(function (response) {
            return response.json();
        });
    };
    PackagesService.prototype.getPackages = function () {
        var url = "/im4booking/package";
        var api_key = localStorage.getItem('api_key');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestMethod */].Get,
            body: {
                'api_key': api_key
            }
        });
        return this.http.request(url, options).toPromise().then(function (response) {
            console.log(response);
            var body = response['_body'];
            console.log("body : " + body);
            return body;
        });
    };
    PackagesService.prototype.getPackage = function (id) {
        var url = "im4booking/package/" + id;
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        headers.append('api_key', api_key);
        headers.append('user_id', user_id);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: headers
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            return response.json();
        });
    };
    PackagesService.prototype.addVouchers = function (data, packageId) {
        var url = "im4booking/package/voucher";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var sdata = {
            'api_key': api_key,
            'user_id': user_id,
            'voucher': data,
            'package_id': packageId
        };
        return this.http.post(url, sdata).toPromise().then(function (response) {
            return response.json();
        });
    };
    PackagesService.prototype.getVoucher = function (package_id) {
        var url = "im4booking/package/voucher/" + package_id;
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        headers.append('api_key', api_key);
        headers.append('user_id', user_id);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: headers
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            return response.json();
        });
    };
    PackagesService.prototype.editVoucher = function (data, packge_id, voucherId) {
        var url = "im4booking/package/voucher/edit/";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var body = {
            'api_key': api_key,
            'user_id': user_id,
            'new_voucher': data,
            'package_id': packge_id,
            'voucher_id': voucherId
        };
        return this.http.post(url, body).toPromise().then(function (response) {
            return response.json();
        });
    };
    PackagesService.prototype.editPackage = function (data, packageId) {
        var url = "im4booking/package/edit/";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var body = {
            'api_key': api_key,
            'user_id': user_id,
            'new_package': data,
            'package_id': packageId
        };
        return this.http.post(url, body).toPromise().then(function (response) {
            return response.json();
        });
    };
    PackagesService.prototype.getVouchers = function () {
        var url = "im4booking/package/voucher/";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        header.append('api_key', api_key);
        header.append('user_id', user_id);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            // console.log(response);
            return response['_body'];
        });
    };
    PackagesService.prototype.sendFile = function (offerId, files) {
        var url = "/upload_pic/package";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        // header.append('Accept', 'multipart/form-data');
        // header.append('Content-Type', 'multipart/form-data');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header
        });
        var formData = new FormData();
        for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
            var file = files_1[_i];
            formData.append('file', file, file.name);
        }
        formData.append('user_id', user_id);
        formData.append('offer_id', offerId);
        formData.append('id', offerId);
        this.http.post(url, formData, options).subscribe();
    };
    PackagesService.prototype.sendFileImgVoucher = function (offerId, files) {
        var url = "/upload_pic/offer";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        // header.append('Accept', 'multipart/form-data');
        // header.append('Content-Type', 'multipart/form-data');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header
        });
        var formData = new FormData();
        for (var _i = 0, files_2 = files; _i < files_2.length; _i++) {
            var file = files_2[_i];
            formData.append('file', file, file.name);
        }
        formData.append('user_id', user_id);
        formData.append('offer_id', offerId);
        formData.append('id', offerId);
        return this.http.post(url, formData, options).toPromise().then(function (res) {
            return res;
        });
    };
    PackagesService.prototype.sendFilePDFVoucher = function (offerId, files) {
        var url = "/upload_media/offer";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        // header.append('Accept', 'multipart/form-data');
        // header.append('Content-Type', 'multipart/form-data');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header
        });
        var formData = new FormData();
        for (var _i = 0, files_3 = files; _i < files_3.length; _i++) {
            var file = files_3[_i];
            formData.append('file', file, file.name);
        }
        formData.append('user_id', user_id);
        formData.append('offer_id', offerId);
        formData.append('id', offerId);
        this.http.post(url, formData, options).subscribe();
    };
    PackagesService.prototype.deletePackage = function (packId) {
        var url = "im4booking/package/delete";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var body = {
            'api_key': api_key,
            'user_id': user_id,
            'package_id': packId
        };
        return this.http.post(url, body).toPromise().then(function (res) {
            return res;
        });
    };
    PackagesService.prototype.deleteVoucher = function (packId) {
        var url = "im4booking/package/voucher/delete";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var body = {
            'api_key': api_key,
            'user_id': user_id,
            'voucher_id': packId
        };
        return this.http.post(url, body).toPromise().then(function (res) {
            return res;
        });
    };
    PackagesService.prototype.deactivatePackage = function (packId) {
        var url = "im4booking/package/deactivate";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var body = {
            'api_key': api_key,
            'user_id': user_id,
            'package_id': packId
        };
        return this.http.post(url, body).toPromise().then(function (res) {
            return res;
        });
    };
    PackagesService.prototype.activatePackage = function (packId) {
        var url = "im4booking/package/activate";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var body = {
            'api_key': api_key,
            'user_id': user_id,
            'package_id': packId
        };
        return this.http.post(url, body).toPromise().then(function (res) {
            return res;
        });
    };
    return PackagesService;
}());
PackagesService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object])
], PackagesService);

var _a;
//# sourceMappingURL=packages.service.js.map

/***/ }),

/***/ "../../../../../src/app/password-redirect/password-redirect.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/password-redirect/password-redirect.component.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"container\">\n  <div class=\"form-group\">\n    <label>Password :</label>\n    <input #name type=\"password\" class=\"form-control\" >\n  </div>\n  <button class=\"btn btn-primary\" (click)=\"changePassword(name.value)\">Submit</button>\n</form>"

/***/ }),

/***/ "../../../../../src/app/password-redirect/password-redirect.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordRedirectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PasswordRedirectComponent = (function () {
    function PasswordRedirectComponent(router, http) {
        this.router = router;
        this.http = http;
    }
    PasswordRedirectComponent.prototype.ngOnInit = function () {
    };
    PasswordRedirectComponent.prototype.changePassword = function (pass) {
        var _this = this;
        console.log(pass);
        var url = "im4booking/user/change_password";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        this.http.post(url, {
            'api_key': api_key,
            'target_userid': user_id,
            'new_password': pass
        }).subscribe(function (res) {
            var r = JSON.parse(JSON.stringify(res));
            console.log(r);
            var body = r['_body'];
            body = JSON.parse(body);
            if (body['valid'] == true) {
                _this.router.navigateByUrl("offer");
            }
        });
    };
    return PasswordRedirectComponent;
}());
PasswordRedirectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-password-redirect',
        template: __webpack_require__("../../../../../src/app/password-redirect/password-redirect.component.html"),
        styles: [__webpack_require__("../../../../../src/app/password-redirect/password-redirect.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* Http */]) === "function" && _b || Object])
], PasswordRedirectComponent);

var _a, _b;
//# sourceMappingURL=password-redirect.component.js.map

/***/ }),

/***/ "../../../../../src/app/requests/requests.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Lato:300,500);", ""]);

// module
exports.push([module.i, "body{\n  font-family: Lato;\n  width: 100%;\n}\n\n.cards-tab{\n  margin: auto;\n  width: 75%;\n}\n\n.delete-button{\n  color: white;\n  background-color: #d50000;\n}\n\n.card-actions{\n  text-align: right;\n}\n\n.view-title{\n  font-size: 40px;\n  font-weight: bolder;\n  text-align: center;\n}\n\n.card-content{\n  text-align: left;\n  margin: auto;\n}\n\n.main-field{\n  margin: auto;\n  font-family: Lato;\n  font-size: 16px;\n  font-weight: bold;\n  height: 30px;\n  width: 100%;\n  border: none;\n  border-bottom: 2px solid #3a08e7;\n}\n\nth {\n  text-align: left;\n  padding-top: .5em;\n  padding-bottom: .5em;\n  width: auto;\n}\n\ntr {\n  margin: 15px;\n  padding: 15px;\n}\n\ntd {\n  width: 100%;\n  text-align: justify;\n}\n\n table {\n   width: 80%;\n   font-family: Lato;\n   font-size: 16px;\n   margin: auto;\n   text-align: center;\n }\n\n.form-actions {\n  text-align: right;\n}\n\n.form-actions button {\n  background-color: #3a08e7;\n  color: white;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/requests/requests.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>\n<md-tab-group>\n  <md-tab label=\"Accepted Requests\">\n    <div class=\"cards-tab\">\n      <h1 class=\"view-title\">Accepted Requests</h1>\n      <div>\n        <label>Paid</label>\n        <input type=\"checkbox\" [(ngModel)]=\"filterPayed\">\n        <label>Unpaid</label>\n        <input type=\"checkbox\" [(ngModel)]=\"filterUnPayed\">\n        <button class=\"btn btn-primary\" (click)=\"filter()\">Search</button>\n      </div>\n      <md-card class=\"package-card\" *ngFor=\"let req of acceptedArray\">\n        <md-card-content>\n          <h3>\n              Request For {{req.request_type}} : {{req.offer}}\n          </h3>\n          <img src=\"{{req.img}}\" alt=\"img\">\n          <h3>\n            From User : {{req.user}}\n          </h3>\n          <h3>\n            Price : {{req.price}}\n          </h3>\n          <h3>\n            Payment Status : {{req.payment_status}}\n          </h3>\n        </md-card-content>\n      </md-card>\n    </div>\n  </md-tab>\n  <md-tab label=\"Pending Requests\">\n      <div class=\"cards-tab\">\n        <h1 class=\"view-title\">Pending Requests</h1>\n        <md-card class=\"package-card\" *ngFor=\"let req of pendingArray\">\n            <md-card-content>\n                <h3>\n                    Request For {{req.request_type}} : {{req.offer}}\n                </h3>\n                <img src=\"{{req.img}}\" alt=\"img\">\n                <h3>\n                  From User : {{req.user}}\n                </h3>\n              </md-card-content>\n          <md-card-actions class=\"card-actions\">\n              <input type=\"number\" #price/>\n              <button class=\"edit-button\" md-button (click)=\"acceptRequest(req.id, price.value)\">Accept</button>\n              <button class=\"delete-button\" md-button (click)=\"rejectRequest(req.id)\">Reject</button>\n          </md-card-actions>\n        </md-card>\n      </div>\n    </md-tab>\n    <md-tab label=\"Rejected Requests\">\n        <div class=\"cards-tab\">\n          <h1 class=\"view-title\">Rejected Requests</h1>\n          <md-card class=\"package-card\" *ngFor=\"let req of rejectedArray\">\n              <md-card-content>\n                  <h3>\n                      Request For {{req.request_type}} : {{req.offer}}\n                  </h3>\n                  <img src=\"{{req.img}}\" alt=\"img\">\n                  <h3>\n                    From User : {{req.user}}\n                  </h3>\n                </md-card-content>\n            <md-card-actions class=\"card-actions\">\n              <button class=\"edit-button\" md-button (click)=\"changeToPending(req.id)\">Change to Pending</button>\n              <button class=\"edit-button\" md-button (click)=\"acceptRequest(req.id)\">Accept</button>\n            </md-card-actions>\n          </md-card>\n        </div>\n      </md-tab>\n</md-tab-group>\n"

/***/ }),

/***/ "../../../../../src/app/requests/requests.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__requests_service__ = __webpack_require__("../../../../../src/app/requests/requests.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__users_user_service__ = __webpack_require__("../../../../../src/app/users/user.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RequestsComponent = (function () {
    function RequestsComponent(requestService, userService) {
        this.requestService = requestService;
        this.userService = userService;
        this.acceptedArray = new Array();
        this.rejectedArray = new Array();
        this.pendingArray = new Array();
        this.tempAccepted = new Array();
    }
    RequestsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.requestService.getAvailableRequests().then(function (res) {
            console.log(res);
            var body = res['response'];
            var acArray = body['accepted'];
            var rejArray = body['rejected'];
            var pendArray = body['pending'];
            acArray.forEach(function (req) {
                if (req['request_type'] == "offer") {
                    _this.userService.getUser(req.user).then(function (res) {
                        console.log("Comp ", res);
                        _this.user = res['response'];
                        console.log("USER ", _this.user);
                        var newRequest = {
                            'user': _this.user.name,
                            'offer': req.offer.name[0].value,
                            'state': req.state,
                            'request_type': req.request_type,
                            'request_date': req.request_date,
                            'id': req._id,
                            'img': req.offer.img_path,
                            'payment_status': req.payment_status,
                            'price': req.price
                        };
                        _this.acceptedArray.push(newRequest);
                        console.log("ARR", _this.pendingArray);
                    });
                }
                else if (req['request_type'] == "voucher") {
                    _this.userService.getUser(req.user).then(function (res) {
                        console.log("Comp ", res);
                        _this.user = res['response'];
                        console.log("USER ", _this.user);
                        var newRequest = {
                            'user': _this.user.name,
                            'offer': req.voucher.name[0].value,
                            'state': req.state,
                            'request_type': req.request_type,
                            'request_date': req.request_date,
                            'id': req._id,
                            'img': req.voucher.img_path,
                            'payment_status': req.payment_status,
                            'price': req.price
                        };
                        _this.acceptedArray.push(newRequest);
                        console.log("ARR", _this.pendingArray);
                    });
                }
                else {
                    _this.userService.getUser(req.user).then(function (res) {
                        console.log("Comp ", res);
                        _this.user = res['response'];
                        console.log("USER ", _this.user);
                        var newRequest = {
                            'user': _this.user.name,
                            'offer': req.package.name[0].value,
                            'state': req.state,
                            'request_type': req.request_type,
                            'request_date': req.request_date,
                            'id': req._id,
                            'img': req.package.img_path,
                            'payment_status': req.payment_status,
                            'price': req.price
                        };
                        _this.acceptedArray.push(newRequest);
                        console.log("ARR", _this.pendingArray);
                    });
                }
            });
            _this.tempAccepted = _this.acceptedArray;
            pendArray.forEach(function (req) {
                if (req['request_type'] == "offer") {
                    _this.userService.getUser(req.user).then(function (res) {
                        console.log("Comp ", res);
                        _this.user = res['response'];
                        console.log("USER ", _this.user);
                        var newRequest = {
                            'user': _this.user.name,
                            'offer': req.offer.name[0].value,
                            'state': req.state,
                            'request_type': req.request_type,
                            'request_date': req.request_date,
                            'id': req._id,
                            'img': req.offer.img_path,
                            'payment_status': req.payment_status,
                            'price': req.price
                        };
                        _this.pendingArray.push(newRequest);
                        console.log("ARR", _this.pendingArray);
                    });
                }
                else if (req['request_type'] == "voucher") {
                    _this.userService.getUser(req.user).then(function (res) {
                        console.log("Comp ", res);
                        _this.user = res['response'];
                        console.log("USER ", _this.user);
                        var newRequest = {
                            'user': _this.user.name,
                            'offer': req.voucher.name[0].value,
                            'state': req.state,
                            'request_type': req.request_type,
                            'request_date': req.request_date,
                            'id': req._id,
                            'img': req.voucher.img_path,
                            'payment_status': req.payment_status,
                            'price': req.price
                        };
                        _this.pendingArray.push(newRequest);
                        console.log("ARR", _this.pendingArray);
                    });
                }
                else {
                    _this.userService.getUser(req.user).then(function (res) {
                        console.log("Comp ", res);
                        _this.user = res['response'];
                        console.log("USER ", _this.user);
                        var newRequest = {
                            'user': _this.user.name,
                            'offer': req.package.name[0].value,
                            'state': req.state,
                            'request_type': req.request_type,
                            'request_date': req.request_date,
                            'id': req._id,
                            'img': req.package.img_path,
                            'payment_status': req.payment_status,
                            'price': req.price
                        };
                        _this.pendingArray.push(newRequest);
                        console.log("ARR", _this.pendingArray);
                    });
                }
            });
            rejArray.forEach(function (req) {
                if (req['request_type'] == "offer") {
                    _this.userService.getUser(req.user).then(function (res) {
                        console.log("Comp ", res);
                        _this.user = res['response'];
                        console.log("USER ", _this.user);
                        var newRequest = {
                            'user': _this.user.name,
                            'offer': req.offer.name[0].value,
                            'state': req.state,
                            'request_type': req.request_type,
                            'request_date': req.request_date,
                            'id': req._id,
                            'img': req.offer.img_path,
                            'payment_status': req.payment_status,
                            'price': req.price
                        };
                        _this.rejectedArray.push(newRequest);
                        console.log("ARR", _this.pendingArray);
                    });
                }
                else if (req['request_type'] == "voucher") {
                    _this.userService.getUser(req.user).then(function (res) {
                        console.log("Comp ", res);
                        _this.user = res['response'];
                        console.log("USER ", _this.user);
                        var newRequest = {
                            'user': _this.user.name,
                            'offer': req.voucher.name[0].value,
                            'state': req.state,
                            'request_type': req.request_type,
                            'request_date': req.request_date,
                            'id': req._id,
                            'img': req.voucher.img_path,
                            'payment_status': req.payment_status,
                            'price': req.price
                        };
                        _this.rejectedArray.push(newRequest);
                        console.log("ARR", _this.pendingArray);
                    });
                }
                else {
                    _this.userService.getUser(req.user).then(function (res) {
                        console.log("Comp ", res);
                        _this.user = res['response'];
                        console.log("USER ", _this.user);
                        var newRequest = {
                            'user': _this.user.name,
                            'offer': req.package.name[0].value,
                            'state': req.state,
                            'request_type': req.request_type,
                            'request_date': req.request_date,
                            'id': req._id,
                            'img': req.package.img_path,
                            'payment_status': req.payment_status,
                            'price': req.price
                        };
                        _this.rejectedArray.push(newRequest);
                        console.log("ARR", _this.pendingArray);
                    });
                }
            });
        });
    };
    RequestsComponent.prototype.filter = function () {
        var _this = this;
        this.acceptedArray = this.tempAccepted.filter(function (acc) {
            if (_this.filterPayed) {
                return acc.payment_status === 'paid';
            }
            else if (_this.filterUnPayed) {
                return acc.payment_status === 'unpaid';
            }
            else {
                return true;
            }
        });
    };
    RequestsComponent.prototype.rejectRequest = function (id) {
        this.requestService.changeRequestStatus(id, "rejected");
    };
    RequestsComponent.prototype.acceptRequest = function (id, price) {
        console.log(typeof price);
        var priceFinal = parseInt(price, 10);
        if (!Number.isNaN(priceFinal)) {
            if (priceFinal > 0) {
                this.requestService.acceptRequest(id, "accepted", priceFinal);
            }
        }
    };
    RequestsComponent.prototype.changeToPending = function (id) {
        this.requestService.changeRequestStatus(id, "pending");
    };
    return RequestsComponent;
}());
RequestsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-requests',
        template: __webpack_require__("../../../../../src/app/requests/requests.component.html"),
        styles: [__webpack_require__("../../../../../src/app/requests/requests.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__requests_service__["a" /* RequestService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__requests_service__["a" /* RequestService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__users_user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__users_user_service__["a" /* UserService */]) === "function" && _b || Object])
], RequestsComponent);

var _a, _b;
//# sourceMappingURL=requests.component.js.map

/***/ }),

/***/ "../../../../../src/app/requests/requests.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RequestService = (function () {
    function RequestService(http) {
        this.http = http;
    }
    ;
    RequestService.prototype.getAvailableRequests = function () {
        var url = "im4booking/request/";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        header.append('api_key', api_key);
        header.append('user_id', user_id);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            console.log(response);
            return JSON.parse(response['_body']);
        });
    };
    RequestService.prototype.changeRequestStatus = function (requestId, newState) {
        var url = "im4booking/request/" + requestId + "/";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        var body = {
            'new_status': newState,
            'user_id': user_id,
            'api_key': api_key
        };
        return this.http.post(url, body).toPromise().then(function (response) {
            return response['body'];
        });
    };
    RequestService.prototype.acceptRequest = function (requestId, newState, price) {
        var url = "im4booking/request/" + requestId + "/";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        var body = {
            'new_status': newState,
            'user_id': user_id,
            'api_key': api_key,
            'new_price': price
        };
        return this.http.post(url, body).toPromise().then(function (response) {
            return response['body'];
        });
    };
    return RequestService;
}());
RequestService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object])
], RequestService);

var _a;
//# sourceMappingURL=requests.service.js.map

/***/ }),

/***/ "../../../../../src/app/termsandconditions/termsandconditions.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>\n<md-tab-group>\n  <md-tab label=\"VIEW ABOUT US \" onclick=\"viewterms()\">\n    <button type=\"button\" name=\"button\"  class=\"btn btn--block card__btn\" (click)= \"viewterms()\">UPDATE</button>\n<ul class=\"cards\">\n  <li class=\"cards__item\">\n    <div class=\"card\">\n      <div class=\"card__content\">\n        <div class=\"card__title\"> About us </div>\n        <p class=\"card__text\">{{terms}} </p>\n      </div>\n    </div>\n  </li>\n</ul>\n  </md-tab>\n  <md-tab label=\"ADD ABOUT US\">\n    <div class='wrap'>\n      <form>\n        <input  placeholder='TERMS' #title>\n        <p>\n        </p>\n        <button class=\"btn btn--block card__btn\" (click)= \"add(title.value)\" >ADD</button>\n      </form>\n    </div>\n  </md-tab>\n\n  </md-tab-group>\n"

/***/ }),

/***/ "../../../../../src/app/termsandconditions/termsandconditions.component.less":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "*,\n*::before,\n*::after {\n  box-sizing: border-box;\n}\nhtml {\n  background-color: #f0f0f0;\n}\nbody {\n  color: #999999;\n  font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  font-style: normal;\n  font-weight: 400;\n  letter-spacing: 0;\n  padding: 1rem;\n  text-rendering: optimizeLegibility;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  -moz-font-feature-settings: \"liga\" on;\n}\nimg {\n  height: auto;\n  max-width: 100%;\n  vertical-align: middle;\n}\n.btn {\n  background-color: white;\n  border: 1px solid #cccccc;\n  color: #2e25e7;\n  padding: 0.5rem;\n  font-weight: 300;\n}\n.btn:hover .btn {\n  -webkit-filter: contrast(10%);\n          filter: contrast(10%);\n}\ninput {\n  font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  font-style: normal;\n  font-weight: 400;\n  font-weight: 300;\n  border: 0;\n  border-bottom: 1px solid #2e25e7;\n  width: 100%;\n  height: 36px;\n  font-size: 26px;\n}\ninput:focus {\n  outline: none;\n  box-shadow: none;\n  background: #5f6de7;\n}\n.wrap {\n  padding: 120px 0;\n  font-size: 62px;\n  color: #888;\n  width: 400px;\n  font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  font-style: normal;\n  font-weight: 400;\n  margin: 0 auto;\n  text-align: center;\n}\n.body {\n  font-family: Lato;\n  width: 100%;\n}\n.btn--block {\n  display: block;\n  width: 100%;\n}\n.cards {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  list-style: none;\n  margin: 0;\n  padding: 0;\n  color: #999999;\n  font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  font-style: normal;\n  font-weight: 400;\n  letter-spacing: 0;\n  text-rendering: optimizeLegibility;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  -moz-font-feature-settings: \"liga\" on;\n}\n.cards__item {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 1rem;\n}\n@media (min-width: 40rem) {\n  .cards__item {\n    width: 50%;\n  }\n}\n@media (min-width: 56rem) {\n  .cards__item {\n    width: 33.3333%;\n  }\n}\n.card {\n  background-color: white;\n  border-radius: 0.25rem;\n  box-shadow: 0 20px 40px -14px rgba(0, 0, 0, 0.25);\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  overflow: hidden;\n}\n.card:hover .card__image {\n  -webkit-filter: contrast(100%);\n          filter: contrast(100%);\n}\n.card__content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 1rem;\n}\n.card__image {\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  border-top-left-radius: 0.25rem;\n  border-top-right-radius: 0.25rem;\n  -webkit-filter: contrast(70%);\n          filter: contrast(70%);\n  overflow: hidden;\n  position: relative;\n  transition: -webkit-filter 0.5s cubic-bezier(0.43, 0.41, 0.22, 0.91);\n  transition: filter 0.5s cubic-bezier(0.43, 0.41, 0.22, 0.91);\n  transition: filter 0.5s cubic-bezier(0.43, 0.41, 0.22, 0.91), -webkit-filter 0.5s cubic-bezier(0.43, 0.41, 0.22, 0.91);\n  /*&::before {\n   content: \"\";\n   display: block;\n   padding-top: 56.25%; // 16:9 aspect ratio\n }*/\n}\n@media (min-width: 40rem) {\n  .card__image::before {\n    padding-top: 66.6%;\n  }\n}\n.card__title {\n  color: #2e25e7;\n  font-size: 1.25rem;\n  font-weight: 300;\n  letter-spacing: 2px;\n  text-transform: uppercase;\n}\n.card__text {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  font-size: 0.875rem;\n  line-height: 1.5;\n  margin-bottom: 1.25rem;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/termsandconditions/termsandconditions.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__termsandconditions_service__ = __webpack_require__("../../../../../src/app/termsandconditions/termsandconditions.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsandconditionsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TermsandconditionsComponent = (function () {
    function TermsandconditionsComponent(service) {
        this.service = service;
    }
    TermsandconditionsComponent.prototype.ngOnInit = function () {
        this.viewterms();
    };
    TermsandconditionsComponent.prototype.viewterms = function () {
        var _this = this;
        this.service.getTerms().then(function (res) {
            _this.esponse = JSON.parse(res)['response'];
            if (JSON.parse(res)['valid']) {
                _this.terms = _this.esponse['data'];
            }
            else {
            }
        });
    };
    TermsandconditionsComponent.prototype.add = function (s) {
        this.service.addterms(s);
    };
    return TermsandconditionsComponent;
}());
TermsandconditionsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-termsandconditions',
        template: __webpack_require__("../../../../../src/app/termsandconditions/termsandconditions.component.html"),
        styles: [__webpack_require__("../../../../../src/app/termsandconditions/termsandconditions.component.less")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__termsandconditions_service__["a" /* TermsandconditionsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__termsandconditions_service__["a" /* TermsandconditionsService */]) === "function" && _a || Object])
], TermsandconditionsComponent);

var _a;
//# sourceMappingURL=termsandconditions.component.js.map

/***/ }),

/***/ "../../../../../src/app/termsandconditions/termsandconditions.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsandconditionsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TermsandconditionsService = (function () {
    function TermsandconditionsService(http, httpPoster) {
        this.http = http;
        this.httpPoster = httpPoster;
        this.valid = true;
        this.header = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]();
    }
    TermsandconditionsService.prototype.getTerms = function () {
        // this.header.append('Content-Type', 'application/json' );
        // this.header.append('Access-Control-Allow-Origin', '*' );
        var url = "im4booking/terms";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var hh = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        hh.append('ahgjjkgfjgfjghfhgfhgfjhgfhjgfghjfjhfjhgfjhgfhjfjhfhjgss', 'asdsasd');
        hh.append('Content-Type', 'application/json');
        hh.append('Access-Control-Allow-Origin', '*');
        hh.append('Access-Control-Allow-Headers', '*');
        hh.append('Access-Control-Allow-Methods', '*');
        hh.append('Access-Control-Allow-Credentials', '*');
        hh.append('Access-Control-Allow-Request-Headers', '*');
        hh.append('Access-Control-Allow-Request-Methods', '*');
        hh.append('Access-Control-Origin', '*');
        hh.append('Accept', 'application/json');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: hh
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            return response['_body'];
        });
    };
    TermsandconditionsService.prototype.addterms = function (id) {
        var _this = this;
        this.header.append('Content-Type', 'application/json');
        this.header.append('Access-Control-Allow-Origin', '*');
        var url = "im4booking/terms";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        this.httpPoster.post(url, {
            'user_id': user_id,
            'api_key': api_key,
            'termsandconditions': id,
        }, {
            headers: this.header
        }).subscribe(function (data) {
            _this.valid = data.valid;
            _this.err = data.msg;
        });
    };
    return TermsandconditionsService;
}());
TermsandconditionsService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */]) === "function" && _b || Object])
], TermsandconditionsService);

var _a, _b;
//# sourceMappingURL=termsandconditions.service.js.map

/***/ }),

/***/ "../../../../../src/app/users/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserService = (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.getAvailableUsers = function () {
        var url = "im4booking/user/";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        header.append('api_key', api_key);
        header.append('user_id', user_id);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            return JSON.parse(response['_body']);
        });
    };
    UserService.prototype.editUser = function (data, id) {
        var url = "im4booking/user/edit";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var body = {
            'user_id': user_id,
            'api_key': api_key,
            'client_id': id,
            'new_user': data
        };
        return this.http.post(url, body).toPromise().then(function (response) {
            return response['requests'];
        });
    };
    UserService.prototype.deleteUser = function (id) {
        var url = "im4booking/user/delete";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var body = {
            'user_id': user_id,
            'api_key': api_key,
            'client_id': id,
        };
        return this.http.post(url, body).toPromise().then(function (response) {
            return response['requests'];
        });
    };
    UserService.prototype.getUser = function (userID) {
        var url = "im4booking/user/" + userID;
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        header.append('api_key', api_key);
        header.append('user_id', user_id);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header
        });
        return this.http.get(url, options).toPromise().then(function (response) {
            var body = response['_body'];
            return JSON.parse(body);
        });
    };
    UserService.prototype.addNewUser = function (data) {
        var url = "im4booking/user";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        return this.http.post(url, {
            'api_key': api_key,
            'user_id': user_id,
            'new_user': data
        }).toPromise().then(function (response) {
            return response;
        });
    };
    UserService.prototype.changeUserPassword = function (userId, newPassword) {
        var url = "/request/" + userId + "/";
        var api_key = localStorage.getItem('api_key');
        var user_id = localStorage.getItem('user_id');
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        header.append('api_key', api_key);
        header.append('user_id', user_id);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: header,
            body: {
                'new_password': newPassword
            }
        });
        return this.http.post(url, options).toPromise().then(function (response) {
            return response;
        });
    };
    return UserService;
}());
UserService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object])
], UserService);

var _a;
//# sourceMappingURL=user.service.js.map

/***/ }),

/***/ "../../../../../src/app/users/users.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Lato:300,500);", ""]);

// module
exports.push([module.i, "body{\n  font-family: Lato;\n  width: 100%;\n}\n\n.cards-tab{\n  margin: auto;\n  width: 75%;\n}\n\n.delete-button{\n  color: white;\n  background-color: #d50000;\n}\n\n.card-actions{\n  text-align: right;\n}\n\n.view-title{\n  font-size: 40px;\n  font-weight: bolder;\n  text-align: center;\n}\n\n.card-content{\n  text-align: left;\n  margin: auto;\n}\n\n.main-field{\n  margin: auto;\n  font-family: Lato;\n  font-size: 16px;\n  font-weight: bold;\n  height: 30px;\n  width: 100%;\n  border: none;\n  border-bottom: 2px solid #3a08e7;\n}\n\nth {\n  text-align: left;\n  padding-top: .5em;\n  padding-bottom: .5em;\n  width: auto;\n}\n\ntr {\n  margin: 15px;\n  padding: 15px;\n}\n\ntd {\n  width: 100%;\n  text-align: justify;\n}\n\n table {\n   width: 80%;\n   font-family: Lato;\n   font-size: 16px;\n   margin: auto;\n   text-align: center;\n }\n\n.form-actions {\n  text-align: right;\n}\n\n.form-actions button {\n  background-color: #3a08e7;\n  color: white;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/users/users.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>\n<button class=\"refresh-button\" md-button (click)=\"refresh()\">Refresh</button>\n<md-tab-group>\n  <md-tab label=\"View Users\">\n    <div class=\"cards-tab\">\n      <h1 class=\"view-title\">Available Users</h1>\n      <input type=\"text\" class=\"form-control\" placeholder=\"Search Users\" [(ngModel)]=\"filterText\">\n      <button class=\"btn btn-primary\" (click)=\"filter()\">Search</button>\n      <md-card class=\"package-card\" *ngFor=\"let user of usersArr\">\n        <md-card-header>\n        </md-card-header>\n        <md-card-content>\n          <div>\n            <label class=\"label-view\">Name</label>\n            <h4 class=\"data-view\">{{user.name}}</h4>\n            <label class=\"label-view\">Email</label>\n            <h4 class=\"data-view\">{{user.email}}</h4>\n            <label class=\"label-view\">Username</label>\n            <h4 class=\"data-view\">{{user.username}}</h4>\n            <label class=\"label-view\">User Type</label>\n            <h4 class=\"data-view\">{{user.user_status}}</h4>\n            <label class=\"label-view\">Phone Number</label>\n            <h4 class=\"data-view\">{{user.phone_number}}</h4>\n            <div *ngIf=\"user.user_type=='client'&&user.client_type!='guest'\">\n              <label class=\"label-view\">Package Expiration Date</label>\n              <h4 class=\"data-view\">{{user.package_exp_date}}</h4>\n            </div>\n          </div>\n          <div *ngIf=\"user.edit_enabled\">\n            <form (ngSubmit)=\"submitUserEdit()\" [formGroup]=\"userFormEdit\">\n              <div class=\"form-group\">\n                <label>Name :</label>\n                <input formControlName=\"name\" type=\"text\" class=\"form-control\" >\n              </div>\n              <div class=\"form-group\">\n                <label for=\"pwd\">Username:</label>\n                <input formControlName=\"userName\" type=\"text\" class=\"form-control\" >\n              </div>\n              <div class=\"form-group\">\n                <label for=\"pwd\">Email:</label>\n                <input formControlName=\"email\" type=\"text\" class=\"form-control\" >\n              </div>\n              <div class=\"form-group\">\n                <label>Password :</label>\n                <input formControlName=\"password\" type=\"text\" class=\"form-control\" >\n              </div>\n              <div class=\"form-group\">\n                <label>Phone Number :</label>\n                <input formControlName=\"phoneNumber\" type=\"number\" class=\"form-control\" >\n              </div>\n              <div class=\"form-group\">\n                <select class=\"selectpicker\" formControlName=\"adminStatus\">\n                  <option value=\"\">Choose admin or client</option>\n                  <option value=\"admin\">admin</option>\n                  <option value=\"client\">client</option>\n                </select>\n              </div>\n              <div class=\"form-group\">\n                <label>Package Name :</label>\n                <select class=\"selectpicker\" formControlName=\"packageId\">\n                  <option value=\"\">Select Package</option>\n                  <option value=\"\">No Package</option>\n                  <option *ngFor=\"let package of packagesArr\" value=\"{{package._id}}\">{{package.name[0].value}}</option>\n                </select>\n              </div>\n              <button class=\"btn btn-primary\" type=\"submit\">Submit</button>\n            </form>\n          </div>\n        </md-card-content>\n        <md-card-actions class=\"card-actions\">\n          <button md-button (click)=\"showPasswordForm(user._id)\" class=\"edit-button\">Edit</button>\n          <button class=\"delete-button\" (click)=\"deleteUser(user._id)\" md-button>Delete</button>\n        </md-card-actions>\n      </md-card>\n    </div>\n  </md-tab>\n  <md-tab label=\"Add New User\">\n    <div class='container'>\n      <form (ngSubmit)=\"submitUser()\" [formGroup]=\"userForm\">\n        <div class=\"form-group\">\n          <label>Name :</label>\n          <input formControlName=\"name\" type=\"text\" class=\"form-control\" >\n        </div>\n        <div class=\"form-group\">\n          <label for=\"pwd\">Username:</label>\n          <input formControlName=\"userName\" type=\"text\" class=\"form-control\" >\n        </div>\n        <div class=\"form-group\">\n          <label for=\"pwd\">Email:</label>\n          <input formControlName=\"email\" type=\"text\" class=\"form-control\" >\n        </div>\n        <div class=\"form-group\">\n          <label>Password :</label>\n          <input formControlName=\"password\" type=\"text\" class=\"form-control\" >\n        </div>\n        <div class=\"form-group\">\n          <label>Phone Number :</label>\n          <input formControlName=\"phoneNumber\" type=\"number\" class=\"form-control\" >\n        </div>\n        <div class=\"form-group\">\n          <select class=\"selectpicker\" formControlName=\"adminStatus\">\n            <option value=\"\">Choose admin or client</option>\n            <option value=\"admin\">admin</option>\n            <option value=\"client\">client</option>\n          </select>\n        </div>\n        <div class=\"form-group\">\n          <label>Package Name :</label>\n          <select class=\"selectpicker\" formControlName=\"packageId\">\n            <option value=\"\">Select Package</option>\n            <option value=\"\">No Package</option>\n            <option *ngFor=\"let package of packagesArr\" value=\"{{package._id}}\">{{package.name[0].value}}</option>\n          </select>\n        </div>\n        <button class=\"btn btn-primary\" type=\"submit\">Submit</button>\n      </form>\n    </div>\n    </md-tab>\n</md-tab-group>\n"

/***/ }),

/***/ "../../../../../src/app/users/users.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_service__ = __webpack_require__("../../../../../src/app/users/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__packages_packages_service__ = __webpack_require__("../../../../../src/app/packages/packages.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_zorro_antd__ = __webpack_require__("../../../../ng-zorro-antd/esm5/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UsersComponent = (function () {
    function UsersComponent(userService, packageService, router, _message) {
        this.userService = userService;
        this.packageService = packageService;
        this.router = router;
        this._message = _message;
        this.changePassword = false;
    }
    UsersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userForm = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormGroup"]({
            name: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            userName: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            email: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            password: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            phoneNumber: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            adminStatus: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            packageId: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]()
        });
        this.userFormEdit = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormGroup"]({
            name: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](''),
            userName: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](''),
            password: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](''),
            email: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            phoneNumber: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](''),
            adminStatus: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](''),
            packageId: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('')
        });
        this.packageService.getPackages().then(function (res) {
            console.log(res);
            var body = JSON.parse(res);
            _this.packagesArr = body['response'];
            console.log(_this.packagesArr);
            _this.userService.getAvailableUsers().then(function (res) {
                console.log("NASDNASD" + res);
                console.log("");
                console.log("RES", res['response']);
                console.log("");
                _this.usersArr = res['response'];
                _this.usersArr = _this.usersArr.map(function (user) {
                    user.edit_enabled = false;
                    if (user.user_type == "admin") {
                        user.user_status = "Admin";
                    }
                    else if (user.user_type == "client" && user.client_type == "guest") {
                        user.user_status = "Guest";
                    }
                    else {
                        console.log("PACKAGEARR", _this.packagesArr);
                        var pack = _this.packagesArr.filter(function (pac) {
                            console.log("PAC IN LOOP", pac);
                            console.log("ID", user.packaged_used);
                            if (user.packaged_used == pac._id) {
                                return pac;
                            }
                        });
                        console.log("PAC SELECTED", pack);
                        if (pack.length == 0) {
                            console.log("IN NO PACKAGE", pack);
                            user.user_status = "Package Deleted";
                        }
                        else {
                            console.log("IN PACKAGE", pack);
                            user.user_status = pack[0].name[0].value;
                        }
                    }
                    return user;
                });
                _this.tempUserArr = _this.usersArr;
            });
        });
    };
    UsersComponent.prototype.filter = function () {
        var _this = this;
        this.usersArr = this.tempUserArr.filter(function (usr) {
            if (_this.filterText.length === 0) {
                return true;
            }
            else {
                return usr.name.includes(_this.filterText);
            }
        });
    };
    UsersComponent.prototype.refresh = function () {
        var _this = this;
        this.packageService.getPackages().then(function (res) {
            console.log(res);
            var body = JSON.parse(res);
            _this.packagesArr = body['response'];
            console.log(_this.packagesArr);
            _this.userService.getAvailableUsers().then(function (res) {
                console.log("NASDNASD" + res);
                console.log("");
                console.log("RES", res['response']);
                console.log("");
                _this.usersArr = res['response'];
                _this.usersArr = _this.usersArr.map(function (user) {
                    user.edit_enabled = false;
                    if (user.user_type == "admin") {
                        user.user_status = "Admin";
                    }
                    else if (user.user_type == "client" && user.client_type == "guest") {
                        user.user_status = "Guest";
                    }
                    else {
                        console.log("PACKAGEARR", _this.packagesArr);
                        var pack = _this.packagesArr.filter(function (pac) {
                            console.log("PAC IN LOOP", pac);
                            console.log("ID", user.packaged_used);
                            if (user.packaged_used == pac._id) {
                                return pac;
                            }
                        });
                        console.log("PAC SELECTED", pack);
                        if (pack.length == 0) {
                            console.log("IN NO PACKAGE", pack);
                            user.user_status = "Package Deleted";
                        }
                        else {
                            console.log("IN PACKAGE", pack);
                            user.user_status = pack[0].name[0].value;
                        }
                    }
                    return user;
                });
                _this.tempUserArr = _this.usersArr;
            });
        });
    };
    UsersComponent.prototype.showPasswordForm = function (id) {
        var _this = this;
        this.changePassword = true;
        this.editUser = id;
        var user = this.usersArr.filter(function (user) {
            if (_this.editUser == user._id) {
                return user;
            }
        });
        console.log("USER USER USER EDIT " + user);
        this.user = user[0];
        this.usersArr = this.usersArr.map(function (user) {
            if (_this.editUser == user._id) {
                user.edit_enabled = true;
            }
            return user;
        });
        this.userFormEdit.patchValue({
            name: this.user.name,
            userName: this.user.username,
            password: this.user.password,
            email: this.user.email,
            phoneNumber: this.user.phone_number,
            adminStatus: this.user.user_type,
            packageId: this.user.packaged_used
        });
    };
    UsersComponent.prototype.editPassword = function (userId) {
    };
    UsersComponent.prototype.deleteUser = function (userId) {
        this.userService.deleteUser(userId);
    };
    UsersComponent.prototype.submitUser = function () {
        var _this = this;
        var data;
        console.log(this.userForm.value.packageId);
        if (this.userForm.value.adminStatus == "admin") {
            data = {
                'name': this.userForm.value.name,
                'username': this.userForm.value.userName,
                'email': this.userForm.value.email,
                'password': this.userForm.value.password,
                'phone_number': this.userForm.value.phoneNumber,
                'user_type': this.userForm.value.adminStatus
            };
        }
        else if (this.userForm.value.adminStatus == "client" && (this.userForm.value.packageId == "" || this.userForm.value.packageId == null)) {
            data = {
                'name': this.userForm.value.name,
                'username': this.userForm.value.userName,
                'email': this.userForm.value.email,
                'password': this.userForm.value.password,
                'phone_number': this.userForm.value.phoneNumber,
                'user_type': this.userForm.value.adminStatus,
                'client_type': 'guest'
            };
        }
        else if (this.userForm.value.adminStatus == "client" && (this.userForm.value.packageId != "" || this.userForm.value.packageId != null)) {
            data = {
                'name': this.userForm.value.name,
                'username': this.userForm.value.userName,
                'email': this.userForm.value.email,
                'password': this.userForm.value.password,
                'phone_number': this.userForm.value.phoneNumber,
                'user_type': this.userForm.value.adminStatus,
                'client_type': 'logged_in',
                'packaged_used': this.userForm.value.packageId
            };
        }
        console.log(data);
        this.userService.addNewUser(data).then(function (status) {
            var body = JSON.parse(status['_body']);
            if (body.valid == true) {
                _this.userForm.reset();
                _this._message.create('success', 'User Added');
            }
            else {
                _this._message.create('error', 'Error !');
            }
        });
    };
    UsersComponent.prototype.submitUserEdit = function () {
        var _this = this;
        var data;
        console.log(this.userForm.value.packageId);
        if (this.userFormEdit.value.adminStatus == "admin") {
            data = {
                'name': this.userFormEdit.value.name,
                'email': this.userFormEdit.value.userName,
                'password': this.userFormEdit.value.password,
                'phone_number': this.userFormEdit.value.phoneNumber,
                'user_type': this.userFormEdit.value.adminStatus
            };
        }
        else if (this.userFormEdit.value.adminStatus == "client" && (this.userFormEdit.value.packageId == "" || this.userFormEdit.value.packageId == null)) {
            data = {
                'name': this.userFormEdit.value.name,
                'username': this.userFormEdit.value.userName,
                'email': this.userFormEdit.value.email,
                'password': this.userFormEdit.value.password,
                'phone_number': this.userFormEdit.value.phoneNumber,
                'user_type': this.userFormEdit.value.adminStatus,
                'client_type': 'guest'
            };
        }
        else if (this.userFormEdit.value.adminStatus == "client" && (this.userFormEdit.value.packageId != "" || this.userFormEdit.value.packageId != null)) {
            data = {
                'name': this.userFormEdit.value.name,
                'email': this.userFormEdit.value.userName,
                'password': this.userFormEdit.value.password,
                'phone_number': this.userFormEdit.value.phoneNumber,
                'user_type': this.userFormEdit.value.adminStatus,
                'client_type': 'logged_in',
                'packaged_used': this.userFormEdit.value.packageId
            };
        }
        console.log("DATA EDIT USER" + data);
        this.userService.editUser(data, this.editUser).then(function (res) {
            _this.router.navigateByUrl("offer");
        });
    };
    return UsersComponent;
}());
UsersComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-users',
        template: __webpack_require__("../../../../../src/app/users/users.component.html"),
        styles: [__webpack_require__("../../../../../src/app/users/users.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__user_service__["a" /* UserService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__packages_packages_service__["a" /* PackagesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__packages_packages_service__["a" /* PackagesService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5_ng_zorro_antd__["b" /* NzMessageService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ng_zorro_antd__["b" /* NzMessageService */]) === "function" && _d || Object])
], UsersComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=users.component.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs__ = __webpack_require__("../../../../hammerjs/hammer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_hammerjs__);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bm": "../../../../moment/locale/bm.js",
	"./bm.js": "../../../../moment/locale/bm.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es-us": "../../../../moment/locale/es-us.js",
	"./es-us.js": "../../../../moment/locale/es-us.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./gu": "../../../../moment/locale/gu.js",
	"./gu.js": "../../../../moment/locale/gu.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map