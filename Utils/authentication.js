
var Promise = require('promise');
var API_Key = require('../models/api_key');
var User = require('../models/user');

var Authentication = {

    api_key_check: function(user_type, api_key) {
        return new Promise(function(resolve, reject) {
            API_Key.find({
                "api_key": api_key,
                "valid_for": user_type
            }, function(err, api_key) {
                if(err) reject(err);
                else {
                    if(api_key.length != 0) {
                        resolve(api_key[0]);
                    }
                    else {
                        reject();
                    }
                }
            });
        });
    },
    get_user_id: function(user_name, password) {
        return new Promise(function(resolve, reject) {
           User.find({
               "username": user_name,
               "password": password
           }, function(err, user) {
               if(err){
                    console.log("error ", err);
                    reject();
               }
               else {
                   if (user.length != 0) {
                       console.log("here in get user id", user[0])
                       resolve(user[0]);
                   }
                   else reject();
               }
           })
        });
    },
    check_admin: function(user_id) {
        return new Promise(function(resolve, reject) {
            console.log("in user promise",user_id);
            User.find({
                "_id": user_id,
                "user_type": "admin"
            }, function(err, user) {
                if(err) {
                    console.log("the error is", err);
                    reject();
                }
                else {
                    console.log("user found ", user);
                    if(user.length != 0) {
                        resolve(user[0]);
                    }
                    else {
                        console.log("error here not found", user);
                        reject();
                    }
                }
            });
        });
    },
    check_user_valid: function(user_id, api_key) {
        return new Promise(function(resolve, reject) {
            User.findById(user_id, function(err, user) {
                if (err) {reject(err);}
                else {
                    var findObj = {
                        "api_key": api_key,
                        "valid_for": user.user_type
                    };
                    API_Key.find(findObj, function(err, key) {
                        if(err) {reject(err);}
                        else {
                            resolve(true);
                        }
                    })
                }
            })
        });
    },
    check_valid_for_all: function(api_key) {
        return new Promise(function(resolve, reject) {
            API_Key.find({"api_key": api_key}, function(err, key) {
                if (err) reject(err);
                else {
                    if (key) {
                        resolve();
                    }
                    else {
                        reject();
                    }
                }
            })
        })
    }
}

module.exports = Authentication;

