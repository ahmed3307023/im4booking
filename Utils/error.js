
var error = function(error_code) {
    this.error_code = error_code;
    this.valid = false;
    this.error_msg = "some error";
}


error.prototype.set_error_code =  function(error_code) {
    this.error_code = error_code;
}
module.exports = error;