

var error_mapper = {
    "data not set" : 0,
    "invalid login" : 1,
    "database connection lost" : 2
};

module.exports = error_mapper;