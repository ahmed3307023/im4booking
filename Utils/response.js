
var respond = function(valid,message,res) {
    this.valid = valid;
    this.msg = message;
    this.response = res;
}


respond.prototype.set_result = function(res) {
    this.response = res;
}
module.exports = respond;