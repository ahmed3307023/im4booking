
var Promise = require('promise');
var fs = require('fs');
var randomstring = require('randomstring');
var reserved_tokens = require('../Strings/reserved_results');
var FCM = require('fcm-node');

var Offer = require('../models/offer');
var News = require('../models/news');
var Package = require('../models/package');


var API_Key = require('../models/api_key');
var User = require('../models/user');

var Helper = {
    api_key_generator: function(user_type) {
        return new Promise(function(resolve, reject) {
            API_Key.find({
                "valid_for": user_type
            }, function(err, api_key) {
                if(err) reject(err);
                else {
                    if (api_key.length != 0) {
                        resolve(api_key[0].api_key);
                    }
                    else {
                        reject();
                    }
                }
            })
        });
    },
    get_user_data: function(user_id) {
        return new Promise(function(resolve, reject) {
            User.findById(user_id, function(err, user) {
                if(err) {
                    console.log(err);
                    reject();
                }
                else resolve(user);
            })
        })
    },
    save_file: function(file, model_name, id, img) {
        return new Promise(function(resolve, reject) {
            var file_temp_path = file.path;
            var file_name = file.originalFilename;
            var file_new_name = randomstring.generate(7) + file_name;
            var file_upload_path =  '../im4booking_data/' + file_new_name;
            fs.readFile(file_temp_path, function(err, data) {
                if (err) {
                    console.log("error in reading");
                    reject(err);
                }
                else {
                    fs.writeFile(file_upload_path, data, function(err) {
                        if (err) {
                            console.log("error in writing files ", err);
                            reject(err);
                        }
                        else {
                            if(model_name == "offer") {
                                Offer.findById(id, function(err, o) {
                                    if(err) {
                                        console.log(err);
                                        reject();
                                    }
                                    else {
                                        if(img) {
                                            o.img_path = 'im4booking_data/' + file_new_name;
                                        }
                                        else {
                                            o.media_path = 'im4booking_data/' + file_new_name;
                                        }
                                        o.save(function(err, obj) {
                                            if (err) {
                                                console.log(err);
                                                reject();
                                            }
                                            else {
                                                resolve(file_new_name);
                                            }
                                        })
                                    }
                                })
                            }
                            else if(model_name == "news") {
                                News.findById(id, function(err, n) {
                                    if (err) reject();
                                    else {
                                        if (img) {
                                            n.img_path = 'im4booking_data/' + file_new_name;
                                        }
                                        else {
                                            n.media_path = 'im4booking_data/' + file_new_name;
                                        }
                                        n.save(function(err, nobj) {
                                            if (err) reject();
                                            else {
                                                resolve(file_new_name);
                                            }
                                        });
                                    }
                                });
                            }
                            else if(model_name == "package") {
                                Package.findById(id, function(err, p) {
                                    if (err) {
                                        reject();
                                    }
                                    else {
                                        if(img) {
                                            p.img_path = 'im4booking_data/' + file_new_name;
                                        }
                                        else {
                                            p.media_path = 'im4booking_data/' + file_new_name;
                                        }
                                        p.save(function(err, pobj) {
                                            if(err) reject();
                                            else {
                                                resolve(file_new_name);
                                            }
                                        })
                                    }
                                });
                            }
                        }
                    });
                }
            });
        });
    },
    get_vouchers: function(vouchers_ids) {
        return new Promise(function(resolve, reject) {
            var find_obj = {};
            find_obj._id = {
                "$in" : vouchers_ids
            };
            Offer.find(find_obj, function(err, vouchers) {
                if (err) reject(err);
                resolve(vouchers);
            });
        });
    },
    get_offers: function(offers_ids) {
        return new Promise(function(resolve, reject) {
            var find_obj = {};
            find_obj._id = {
                "$in" : offers_ids
            };
            Offer.find(find_obj, function(err, offers) {
                if (err) reject(err);
                resolve(offers);
            });
        });
    },
    request_data_promise: function(request) {
        return new Promise(function(resolve, reject) {
            if(request.request_type == "offer") {
                Offer.findById(request.offer, function(err, of) {
                    if(err) reject(err);
                    request.offer = of;
                    resolve(request);
                });
            }
            else if(request.request_type == "package") {
                Package.findById(request.package, function(err, pac) {
                    if(err) reject(err);
                    request.package = pac;
                    resolve(request);
                });
            }
            else if(request.request_type == "voucher") {
                Offer.findById(request.voucher, function(err, voch) {
                    if(err) reject(err);
                    request.voucher = voch;
                    resolve(request);
                })
            }
            else {
                reject();
            }
        });
    },
    voucher_in_package: function(package_id, voucher_id) {
        console.log("here in helper");
        return new Promise(function(resolve, reject) {

            var find_obj = {};
            find_obj._id = package_id;
            find_obj.vouchers = {
                "$in" : [voucher_id]
            };
            Package.find(find_obj, function(err, package) {
                if(err) reject();
                if(package.length == 1) {
                    console.log("here in good helper")
                    resolve();
                }
                else reject();
            })
        })
    },
    notify_user_request_status_change: function(user, request) {
        var fcm = new FCM(reserved_tokens.server_name);
        var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            "to": user.reg_id,
            "collapse_key": 'trial',
            "notification": {
                title: "Request Approval",
                body: "Your request with ID " + request._id + " is approved now.",
                sound: "default"
            },
            "data": request,
            "priority": "high",
            "content-available": true
        };

        fcm.send(message, function(err, response){
            if (err) {
                console.log("Error in fcm");
                console.log(err);
                callback(false);
            } else {
                console.log("Send msg");
                callback(true);
            }
        });
    },
    notifyNews: function(news) {
        return new Promise(function(resolve, reject) {
            var fcm = new FCM(reserved_tokens.server_name);
            var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                "to": '/topics/news',
                "collapse_key": 'trial',
                "notification": {
                    title: news.title[1]['value'],
                    body: news.body[1]['value'],
                    sound: "default"
                },
                "data": news,
                "priority": "high",
                "content-available": true
            };

            fcm.send(message, function(err, response){
                if (err) {
                    console.log(err);
                    reject();
                } else {
                    console.log("Send msg");
                    resolve(response)
                }
            });
        })
    }
};

module.exports = Helper;
