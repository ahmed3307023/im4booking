

var reserved_tokens = {
    login_status: {
        new_login: "new login",
        current_user: "current user",
        logout: "logout",
    },
    user_types: {
        admin: "admin",
        client: "client"
    },
    offer_status: {
        pending: "pending",
        accepted: "accepted",
        rejected: "rejected"
    },
    payment_status: {
        paid: "paid",
        unpaid: "unpaid"
    },
    upload_dir:  "/home/root/im4booking_data",  //process.env.OPENSHIFT_DATA_DIR + "/uploads/",
    server_name: "AAAAt3tJGbg:APA91bG3lT-oinehUOOrS7fCZmyki0E2vzyBQu3RSkCOqpce60KRlKHn0res6JugjZ2k7LX6KXuQWhMeQmjjjTmqilbuBwD7L22bHJG-PEKGL5qf8dqNDVVZ44TrlfdDQC35w2q8u8yS"
};

module.exports = reserved_tokens;