/**
 * Created by ahmedalaa on 8/12/17.
 */

var mongoose = require('mongoose');

var Strings = require('../Strings/reserved_results')

var Contact_Us_Schema = mongoose.Schema(
    {
        phone_number: {
            type: String
        },
        email: {
            type: String,
        }
    }
);
var Contacts = mongoose.model('Contacts', Contact_Us_Schema);
module.exports = Contacts;