/**
 * Created by ahmedalaa on 8/12/17.
 */

var mongoose = require('mongoose');

var reserved_tokens = require('../Strings/reserved_results');

var request_schema = mongoose.Schema(
    {
        request_date: {
            type: Date
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        offer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Offer'
        },
        package: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Package'
        },
        voucher: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Offer'
        },
        status: {
            type: String,
            default: reserved_tokens.offer_status.pending
        },
        request_type: {
            type: String
        },
        payment_status: {
            type: String,
            default: reserved_tokens.payment_status.unpaid
        },
        price: {
            type: Number,
            default: 0
        }
    }
);
var Request = mongoose.model('Request', request_schema);
module.exports = Request;