/**
 * Created by ahmedalaa on 8/12/17.
 */

var mongoose = require('mongoose');

var news_schema = mongoose.Schema(
    {
       title: [
            {
              lang: {
                type: String,
                required: true
              },
              value:{
                type: String,
                required: true
              }
            }
       ],
       body: [
            {
                lang: {
                    type: String,
                    required: true
                },
                value:{
                    type: String,
                    required: true
                }
            }
       ],
        date: {
            type: Date
        },
        img_path: {
            type: String
        },
        media_path: {
            type: String
        }
    }
);
var News = mongoose.model('News', news_schema);
module.exports = News;