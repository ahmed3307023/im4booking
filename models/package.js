/**
 * Created by ahmedalaa on 8/12/17.
 */

var mongoose = require('mongoose');

var package_schema = mongoose.Schema(
    {
        name: [
             {
               lang: {
                 type: String,
                 required: true
               },
               value:{
                 type: String,
                 required: true
               }
             }
        ],
        description: [
             {
               lang: {
                 type: String,
                 required: true
               },
               value:{
                 type: String,
                 required: true
               }
             }
        ],
        listed_customer: {
            type: [mongoose.Schema.Types.ObjectId],
            ref: 'User'
        },
        vouchers: {
            type: [mongoose.Schema.Types.ObjectId],
            ref: 'Offer'
        },
        points: {
            type: Number
        },
        activate: {
            type: Boolean,
            default: true
        },
        img_path: {
            type: String
        }
    }
);
var Package = mongoose.model('Package', package_schema);
module.exports = Package;