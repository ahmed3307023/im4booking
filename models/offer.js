/**
 * Created by ahmedalaa on 8/12/17.
 */

var mongoose = require('mongoose');

var offer_schema = mongoose.Schema(
    {
        name:[
            {
              lang: {
                type: String,
                required: true
              },
              value:{
                type: String,
                required: true
              }
            }
        ],
        description: [
             {
               lang: {
                 type: String,
                 required: true
               },
               value:{
                 type: String,
                 required: true
               }
             }
        ],
        exp_date: {
            type: Date
        },
        creation_date: {
            type: Date
        },
        given_points: {
            type: Number,
            required: true,
            default: 0
        },
        img_path: {
            type: String
        },
        media_path: {
            type: String
        },
        condition_type: {
            type: Boolean
            //required: true
        },
        condition: [
           {
             lang: {
               type: String,
               required: true
             },
             value:{
               type: String,
               required: true
             }
           }
        ],
        price: {
            type: Number
        },
        used_one: {
            type: Boolean
        },
        is_voucher: {
            type: Boolean,
            default: false,
            required: true
        },
        in_package: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Package'
        }
    }
);


var Offer = mongoose.model('Offer', offer_schema);
module.exports = Offer;