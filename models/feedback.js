/**
 * Created by ahmedalaa on 8/12/17.
 */

var mongoose = require('mongoose');

var feedback_schema = mongoose.Schema(
    {
        about: {
            type: String
        },
        details: {
            type: String
        },
        from_user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        date: {
            type: Date
        }
    }
);
var Feedback = mongoose.model('Feedback', feedback_schema);
module.exports = Feedback;