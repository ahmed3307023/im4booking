/**
 * Created by ahmedalaa on 8/12/17.
 */

var mongoose = require('mongoose');

var Strings = require('../Strings/reserved_results')

var User_schema = mongoose.Schema(
    {
        name: {
            type: String,
            required: String
        },
        fcm_user_id: {
            type: String
        },
        email: {
            type: String,
            required: true,
            unique: true,
            index: true,
        },
        username: {
            type: String
        },
        phone_number: {
            type: String
        },
        login_status: {
            type: String,
            default: Strings.login_status.new_login
        },
        user_type: { // admin or client
            type: String,
            required: true
        },
        password: {
            type: String
        },
        client_type: { //loged in user or guest
            type: String,
        },
        points: {
            type: Number
        },
        package_exp_date: {
            type: Date
        },
        package_used_voucher: {
            type: [mongoose.Schema.Types.ObjectId],
            ref: 'Offer'
        },
        packaged_used: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Package'
        },
        used_offers: {
            type: [mongoose.Schema.Types.ObjectId],
            ref: 'Offer'
        }
    }
);
var User = mongoose.model('Userْ', User_schema);
module.exports = User;