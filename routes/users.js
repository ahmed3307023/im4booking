var express = require('express');
var router = express.Router();

var Error_mapper = require('../Utils/error_mapper');
var Error = require('../Utils/error');
var Authentication  = require('../Utils/authentication');
var Helper = require('../Utils/helper');

var Response = require('../Utils/response')
var Strings = require('../Strings/reserved_results');
var User = require('../models/user');

router.post('/login', function(req, res, next) {
    var request_body = req.body;
    if(typeof request_body.user_name != 'undefined' && typeof request_body.password != 'undefined') {
        Authentication.get_user_id(request_body.user_name, request_body.password)
        .then(function(user_data) {
            console.log("here there is a user");
            Helper.api_key_generator(user_data.user_type)
            .then(function(api_key) {
                console.log("here just generate a key");
                var result = {
                    "api_key": api_key,
                    "user_id": user_data._id,
                    "user_type": user_data.user_type,
                    "login_status": user_data.login_status
                };
                console.log(result);
                res.json(new Response(true,"correct operation",result));
            })
            .catch(function(err) {
                console.log("error in new Response");
                res.json(new Error(err));
            })
        })
        .catch(function() {
            console.log("error in new Response outer");
            res.json(new Error("error"));
        });
    }
    else {
        res.json(new Error(Error_mapper["data not set"]));
    }
});

router.post('/change_password', function(req, res, next) {
    var request_body = req.body;
    if(typeof request_body.api_key != 'undefined' && typeof request_body.target_userid != 'undefined'
            && typeof request_body.new_password != 'undefined') {
        Helper.get_user_data(request_body.target_userid)
        .then(function(user_data) {
            Authentication.api_key_check(user_data.user_type, request_body.api_key)
            .then(function(api_key) {
                user_data.password = request_body.new_password;
                user_data.login_status = Strings.login_status.current_user;
                user_data.save(function(err, result) {
                    if (err) res.json(new Error(err));
                    else res.json(new Response(true,"correct operation"));
                });
            })
            .catch(function(){
                res.json(new Error("some error"));
            })
        })
        .catch(function() {
            res.json(new Error("some other error"));
        })
    }
    else {
        res.json(new Error(Error_mapper["data not set"]));
    }

});

router.get('/:id', function(req, res, next) {
    var get_id = req.params.id;
    var request_body = req.headers;
    if(typeof request_body.api_key != 'undefined' && typeof request_body.user_id != 'undefined') {
        Helper.get_user_data(request_body.user_id)
        .then(function(user_data) {
            Authentication.api_key_check(user_data.user_type, request_body.api_key)
            .then(function(api_key) {
                if(user_data.user_type == Strings.user_types.admin) {
                    console.log("reached here", api_key);

                    Helper.get_user_data(get_id)
                    .then(function(target_data) {
                        res.json(new Response(true, "correct operation", target_data))
                    })
                    .catch(function() {
                        res.json(new Error("unhandled error"));
                    })
                }
                else {
                    if(get_id == user_data._id) {
                        res.json(new Response(true, "correct operation", user_data));
                    }
                    else {
                        res.json(new Error("unauthorized action"));
                    }
                }
            })
            .catch(function() {
                res.json(new Error("some error 2"));
            })
        })
        .catch(function() {
            res.json(new Error("error is here"));
        })
    }
    else {
        res.json(new Error(Error_mapper["data not set"]));
    }
});

router.get('/update_fcm_id/:user_id', function(req, res, next) {
    var user_id = req.params.user_id;
    var reg_id = req.headers.fcm_user_id;
    User.findById(user_id, function(err, user) {
        if (err) {
            res.json(new Error("not valid operation"));
        }
        else {
            user.fcm_user_id = reg_id;
            user.save(function(err, u) {
                if (err) {
                    res.json(new Error("Could not update"));
                }
                else {
                    res.json(new Response(true, "correct operation", u));
                }
            })
        }
    });
});
// for admin use only
router.post('/', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var new_user =  req.body.new_user;
    console.log("the user", new_user);
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined') {
        Authentication.check_admin(user_id)
        .then(function(user) {
            console.log("the new user will be created");

            var u = new User(new_user);
            if (u.user_type == 'client' && u.client_type != 'guest') {
                var ex = new Date(Date.now());
                ex.setFullYear(ex.getFullYear() + 1);
                u.package_exp_date = ex;
            }
            u.save(function(err, us) {
                if (err) res.json(new Error("db error"));
                else {
                    res.json(new Response(true, "correct operation", us));
                }
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/edit', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var new_user = req.body.new_user;
    var client_id = req.body.client_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined' && typeof client_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                User.findById(client_id, function(err, user_obj) {
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        for (var field in new_user) {
                            user_obj[field] = new_user[field];
                        }
                        user_obj.save(function(err, r) {
                            if (err) {
                                res.json(new Error("DB error in saving"));
                            }
                            else {
                                res.json(new Response(true, "correct operation", r._id));
                            }
                        });
                    }
                })
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/delete', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var client_id = req.body.client_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined' && typeof client_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                User.remove({'_id':client_id}, function(err, r){
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        res.json(new Response(true, "correct operation"));
                    }
                });
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.get('/', function(req, res, next) {
    User.find({}, function(err, r) {
        if (err) {
            res.json(new Error("Error"));
        }
        else {
            res.json(new Response(true, "correct operation", r));
        }
    })
})
module.exports = router;