var express = require('express');
var router = express.Router();

var User = require('../models/user');
var Api = require('../models/api_key');
var Request = require('../models/request');
var Authentication = require('../Utils/authentication');
var Error = require('../Utils/error');
var Authentication  = require('../Utils/authentication');
var Helper = require('../Utils/helper');
var Response = require('../Utils/response')
var Strings = require('../Strings/reserved_results');
var Contact = require('../models/contactus');

router.post('/', function(req, res, next) {

})

router.get('/', function(req, res, next) {
    Contact.find({}, function(err, r) {
        if (err) res.json(new Error("no contact results"));
        else {
            res.json(new Response(true, "correct operation", r));
        }
    });
})


module.exports = router;