var express = require('express');
var router = express.Router();

var User = require('../models/user');
var Api = require('../models/api_key');
var Request = require('../models/request');
var Authentication = require('../Utils/authentication');
var Error = require('../Utils/error');
var Authentication  = require('../Utils/authentication');
var Helper = require('../Utils/helper');
var Response = require('../Utils/response')
var Strings = require('../Strings/reserved_results');

/* GET home page. */
router.get('/', function(req, res, next) {
    var user_id = req.headers.user_id;
    var api_key = req.headers.api_key;

    if (typeof user_id != 'undefined' && typeof api_key != 'undefined') {
        Helper.get_user_data(user_id)
        .then(function(user) {
            if (user) {
                Authentication.api_key_check(user.user_type, api_key)
                .then(function(api) {
                    if (api) {
                        Helper.get_vouchers(user.package_used_voucher)
                        .then(function(vouchers) {
                            user.package_used_voucher = vouchers;
                            Helper.get_offers(user.used_offers)
                            .then(function(offers) {
                                user.used_offers = offers;
                                    res.json(new Response(true, "correct operation", user));
                            })
                            .catch(function(err) {
                                res.json(new Error("db error"));
                            })
                        })
                        .catch(function(err) {
                            res.json(new Error("db error"));
                        })
                    }
                    else {
                        res.json(new Error("not valid api"))
                    }
                })
            }
            else {
                res.json(new Error("user not found"));
            }
        })
        .catch(function(err) {
            res.json(new Error("error in getting user data"));
        })
    }
    else {
        res.json(new Error("data not correct"));
    }
});

router.get('/history/', function(req, res, next) {
    var user_id = req.headers.user_id;
    var api_key = req.headers.api_key;

    if(typeof user_id != 'undefined' && typeof api_key != 'undefined') {
        Helper.get_user_data(user_id)
        .then(function(user) {
            if (user) {
                Authentication.api_key_check(user.user_type, api_key)
                .then(function(api) {
                    Request.find({
                        user:user_id,
                        request_type: {
                            "$in" : ["offer", "voucher"]
                        }
                    }, function(err, results) {
                        if (err) {
                            res.json(new Error("error in using data"));
                        }
                        else {
                            if(results.length != 0) {
                                results = results.map(function(item) {
                                    return (Helper.request_data_promise(item));
                                });
                                Promise.all(results)
                                .then(function(results_data) {
                                    res.json(new Response(true, "correct operation", results_data));
                                })
                                .catch(function(err) {
                                    res.json(new Error("wrong in getting the data"));
                                });
                            }
                            else {
                                res.json(new Response(true, "correct operation", results));
                            }
                        }
                    })
                })
                .catch(function(err) {
                    res.json(new Error("api key not valid"));
                })
            }
            else {
                res.json(new Error("error in user data"));
            }
        })
        .catch(function(err) {
            res.json(new Error("user not found"));
        })
    }
    else {
        res.json(new Error("data not correct"));
    }

});

router.post('/', function(req, res, next) {
    var request_body = req.body;
    if(typeof request_body.email != 'undefined' && typeof request_body.name != 'undefined' && typeof request_body.phone != 'undefined') {
        var u = {
            "name": request_body.name,
            "email": request_body.email,
            "username": request_body.username,
            "phone_number": request_body.phone,
            "password": request_body.password,
            "user_type": "client",
            "client_type": "guest"
        };
        var user = User(u);
        user.save(function(err, us) {
            if(err) {
                res.json(new Error("DB error"));
            }
            else {
                res.json(new Response(true, "correct operation", us));
            }
        });
    }
    else {
        res.json(new Error("Not sufficient data"));
    }
});
module.exports = router;
