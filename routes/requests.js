var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Error = require('../Utils/error');
var Authentication  = require('../Utils/authentication');
var Helper = require('../Utils/helper');
var Response = require('../Utils/response');
var Request = require('../models/request');
var Promise = require('promise');
var reserved_tokens = require('../Strings/reserved_results');

router.post('/offer', function(req, res, next) {
    var request_data = req.body;
    if (typeof request_data.user_id != 'undefined' && typeof request_data.offer_id != 'undefined') {
        var user_id = request_data.user_id;
        var offer_id = request_data.offer_id;
        Helper.get_user_data(user_id)
        .then(function(user) {
            if(user) {
                var user_request = {
                    "request_date": new Date(Date.now()),
                    "user": user_id,
                    "offer": offer_id,
                    "request_type": "offer",
                    "payment_status": reserved_tokens.payment_status.unpaid
                };
                var request_obj = new Request(user_request);
                request_obj.save(function(err, obj) {
                    if (err) {
                        res.json(new Error("error in saving the request"));
                    }
                    else {
                        res.json(new Response(true, "correct operation", obj._id));
                    }
                });
            }
            else {
                res.json(new Error("User not found"));
            }
        })
        .catch(function() {
            res.json(new Error("Not a user"));
        });
    }
    else {
        res.json(new Error("can not find user id or offer id"));
    }
});

router.post('/membership', function(req, res, next) {
    var request_data = req.body;
    if (typeof request_data.user_id != 'undefined' && typeof request_data.package_id != 'undefined') {
        var user_id = request_data.user_id;
        var package_id = request_data.package_id;
        Helper.get_user_data(user_id)
        .then(function(user) {
            if(user) {
                var user_request = {
                    "request_date": new Date(Date.now()),
                    "user": user_id,
                    "package": package_id,
                    "request_type": "package",
                    "payment_status": reserved_tokens.payment_status.unpaid
                };
                var request_obj = new Request(user_request);
                request_obj.save(function(err, obj) {
                    if (err) {
                        res.json(new Error("error in saving the request"));
                    }
                    else {
                        res.json(new Response(true, "correct operation", obj._id));
                    }
                });
            }
            else {
                res.json(new Error("User not found"));
            }
        })
        .catch(function() {
            res.json(new Error("Not a user"));
        });
    }
    else {
        res.json(new Error("can not find user id or offer id"));
    }
});

router.post('/package/voucher', function(req, res, next) {
    var request_data = req.body;
    if (typeof request_data.user_id != 'undefined' && typeof request_data.voucher_id != 'undefined'
       && typeof request_data.package_id != 'undefined') {
        var user_id = request_data.user_id;
        var voucher_id = request_data.voucher_id;
        var package_id = request_data.package_id;
        Helper.get_user_data(user_id)
        .then(function(user) {
            if(user) {
                if (user.package_exp_date >= new Date(Date.now())) {
                    Helper.voucher_in_package(package_id, voucher_id)
                    .then(function() {
                        if(user.packaged_used.equals(package_id)) {
                            var i = 0;
                            for(i = 0; i < user.package_used_voucher.length; i++){
                                if(user.package_used_voucher[i].equals(voucher_id)){
                                    break;
                                }
                            }
                            if(i == user.package_used_voucher.length){
                                console.log("here againhgfjhgfgjhfjhgfjghfjhg and again");
                                var user_request = {
                                    "request_date": new Date(Date.now()),
                                    "user": user_id,
                                    "voucher": voucher_id,
                                    "request_type": "voucher",
                                    "payment_status": reserved_tokens.payment_status.unpaid
                                };

                                var request_obj = new Request(user_request);
                                request_obj.save(function(err, obj) {
                                    if (err) {
                                        console.log("error in requesting voucher", err)
                                        res.json(new Error("error in saving the request"));
                                    }
                                    else {
                                        res.json(new Response(true, "correct operation", obj._id));
                                    }
                                });
                            }
                            else{
                            console.log("here again and again");
                                Helper.get_voucher(voucher_id)
                                .then(function(voucher_data) {

                                    if(voucher_data.used_one){
                                        res.json(new Error("Already used this voucher"));
                                    }
                                    else{
                                        var user_request = {
                                            "request_date": new Date(Date.now()),
                                            "user": user_id,
                                            "voucher": voucher_id,
                                            "request_type": "voucher",
                                            "payment_status": reserved_tokens.payment_status.unpaid
                                        };
                                        var request_obj = new Request(user_request);
                                        request_obj.save(function(err, obj) {
                                            if (err) {
                                                console.log("error in requesting voucher", err)
                                                res.json(new Error("error in saving the request"));
                                            }
                                            else {
                                                res.json(new Response(true, "correct operation", obj._id));
                                            }
                                        });
                                    }
                                })
                                .catch(function() {
                                    res.json(new Error("voucher data not found"));
                                })
                            }
                        }
                        else {

                            res.json(new Error("Not valid action"));
                        }
                    })
                    .catch(function() {
                        res.json(new Error("Not valid request"));
                    })
                }
                else {
                    res.json(new Error("Package already expired"));
                }
            }
            else {
                res.json(new Error("User not found"));
            }
        })
        .catch(function() {
            res.json(new Error("Not a user"));
        });
    }
    else {
        res.json(new Error("can not find user id or offer id"));
    }
})

router.get('/unpaid', function(req, res, next) {
    var user_id = req.headers.user_id;
    var search_obj = {};
    search_obj.user = user_id;
    search_obj.payment_status = reserved_tokens.payment_status.unpaid;
    search_obj.status = reserved_tokens.offer_status.accepted;
    Request.find(search_obj, function(err, results) {
        if(err)
            res.json(new Error("invalid api key"));
        else {
            requests = results.map(function(r) {
                return Helper.request_data_promise(r);
            });
            var data = [];
            Promise.all(requests)
            .then(function(requests) {
                data = requests.map(function(r) {
                    console.log(r);
                    var obj = {
                        "id" : r._id,
                        "type" : r.request_type,
                        "price" : r.price
                    };
                    if(r.request_type == "offer") {
                        obj.name = r.offer.name;
                    }
                    else if(r.request_type == "voucher") {
                        obj.name = r.voucher.name;
                    }
                    else if(r.request_type == "package") {
                        obj.name = r.package.name;
                    }
                    return obj;
                });
                res.json(new Response(true, "correct operation", data));
            })
            .catch(function(err) {
                res.json(new Error("man error"));
            })

        }
    })
})

router.post('/transaction', function(req, res, next) {

    Request.findById(req.body.merchant_extra1, function(err, r) {
        if(err) {
            res.json({
                "error": "error"
            });
        }
        else {
            r.payment_status = reserved_tokens.payment_status.paid;
            r.save(function(err, saved) {
                if(err) {
                    res.json({
                        "message" : "error in saving the transaction",
                         "error" : true
                     });
                }
                else {
                    res.json({
                        "message" : true,
                         "error" : null
                    });
                }
            });
        }
    });
})

// for admin use only
router.get('/', function(req, res, next) {
    var api_key = req.headers.api_key;
    var user_id = req.headers.user_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined') {
        Authentication.api_key_check(reserved_tokens.user_types.admin, api_key)
        .then(function(api) {
            if (api) {
                Request.find({}, function(err, requests) {
                    if (err) {
                        res.json(new Error("Database error"));
                    }
                    else {
                        if(requests.length != 0) {
                            var data = {};
                            requests = requests.map(function(r) {
                                return Helper.request_data_promise(r);
                            });
                            Promise.all(requests)
                            .then(function(requests) {
                                data[reserved_tokens.offer_status.pending] = requests.filter(function(request) {
                                    if (request.status == reserved_tokens.offer_status.pending) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                });
                                data[reserved_tokens.offer_status.accepted] = requests.filter(function(request) {
                                    if (request.status == reserved_tokens.offer_status.accepted) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                });
                                data[reserved_tokens.offer_status.rejected] = requests.filter(function(request) {
                                    if (request.status == reserved_tokens.offer_status.rejected) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                });
                                res.json(new Response(true, "correct operation", data));
                            })
                            .catch(function(err) {
                                res.json(new Error("man error"));
                            })
                        }
                        else {
                            res.json(new Response(true, "correct operation", requests))
                        }
                    }
                })
            }
            else {
                res.json(new Error("Unauthorized operation"));
            }
        })
        .catch(function() {
            res.json(new Error("Not valid"));
        })
    }
    else {
        res.json(new Error("Data not enough"));
    }
});

// change request status for admin use only
router.post('/:request_id', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var new_status = req.body.new_status;
    var new_price = req.body.new_price;

    var request_id = req.params.request_id;

    if (typeof api_key != 'undefined' && typeof user_id != 'undefined' && typeof new_status != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                Request.findById(request_id, function(err, request) {
                    if (err) res.json(new Error("db error"));
                    else {
                        request.status = new_status;
                        request.price = new_price;
                        if (request.request_type == "package" && request.status == "accepted") {
                            Helper.get_user_data(request.user)
                            .then(function(client) {
                                var ex = new Date(Date.now());
                                ex.setFullYear(ex.getFullYear() + 1);
                                client.package_exp_date = ex;
                                client.packaged_used = request.package;
                                client.save(function(err, resul) {
                                    if (err) {
                                        res.json(new Error("Can not save user data"));
                                    }
                                    else {
                                        request.save(function(err, r) {
                                            if(err) res.json(new Error("db error"));
                                            else {
                                                res.json(new Response(true, "correct operation", r));
                                            }
                                        });
                                    }
                                });
                            })
                            .catch(function(err) {
                                res.json(new Error("User not found"));
                            })
                        }
                        else {
                             request.save(function(err, r) {
                                 if(err) res.json(new Error("db error"));
                                 else {
                                     res.json(new Response(true, "correct operation", r));
                                 }
                             });
                        }
                    }
                });
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }

});

module.exports = router;
