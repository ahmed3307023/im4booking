var express = require('express');
var router = express.Router();

var cors = require('cors');
var path = require('path');
var multiparty = require('connect-multiparty');

var User = require('../models/user');
var Feedback = require('../models/feedback');
var Package = require('../models/package');
var Offer = require('../models/offer');
var API_Key = require('../models/api_key');

var Error = require('../Utils/error');
var Response = require('../Utils/response');
var Helper = require('../Utils/helper');

var Strings = require('../Strings/reserved_results');
var randomstring = require('randomstring');
//
//var multer  = require('multer');
//    var storage = multer.memoryStorage();
//    var upload = multer({ storage: storage });

router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname, '../', 'views', 'index.html'));
});


router.post('/add_user', function(req, res, next) {
    var user = new User({
        "email": "ahmed",
        "username": "ahmed",
        "password": "alaa",
        "name": "Ahmed Alaa",
        "user_type": "admin",
        "login_status": Strings.login_status.new_login
    });
    user.save(function(err, r) {
        if(err) res.json(err);
        else res.json(r);
    })
});

router.post('/add_api', function(req, res, next) {
    var api_key = new API_Key({
        "api_key": randomstring.generate(32),
        "created_date": new Date(Date.now()),
        "valid_for": Strings.user_types.client
    });
    api_key.save(function(err, api) {
        if(err) res.json(err);
        else res.json(api);
    });
});

router.post('/add_of/', multiparty(),function(req, res, next) {

    console.log(req.file);
    console.log(req);
    console.log(req.files);
    console.log("here in uploading ff");
    res.json({"ahmed":"trial let it work mohamed Ahmed"});
});

router.post('/add_feedback', function(req, res, next) {
    var f = {
        about: "Trial Ahmed",
        details: "the body",
        date: new Date(Date.now())
    }
    var feed = new Feedback(f);
    feed.save(function(err, ff) {
        if (err) res.json(new Error("error"));
        else {
            res.json(new Response(true, "correct operation", ff));
        }
    })
})

router.post('/upload_pic/:modelName', multiparty(), function(req, res, next) {
    var modelName = req.params.modelName;
    var id = req.body.id;
    console.log("the body is ", req.body);
    Helper.save_file(req.files.file, modelName, id, true)
    .then(function(file_path) {
        var r = {
            "upload_path": 'im4booking_data/' + file_path
        }
        res.json(new Response(true, "correct_operation", r));
    })
    .catch(function(err) {
        console.log(err);
        res.json(new Error("can not upload that file"));
    })
});

router.post('/upload_media/:modelName', multiparty(), function(req, res, next) {
    var modelName = req.params.modelName;
    var id = req.body.id;
    console.log(req);
    Helper.save_file(req.files.file, modelName, id, false)
    .then(function(file_path) {
        var r = {
            "upload_path": 'im4booking_data/' + file_path
        }
        res.json(new Response(true, "correct_operation", r));
    })
    .catch(function(err) {
        res.json(new Error("can not upload that file"));
    })
});

router.get('/link_voucher_img', function(req, res, next) {
    Package.find({}, function(err,packs) {
        if (err) {
            res.json(new Error("DB error"));
        }
        else {
            packs.forEach(function(pack) {
                var v = pack.vouchers;
                var img = pack.img_path;
                var id = pack._id;
                v.forEach(function(voucher) {
                    Offer.findById(voucher, function(err, vr) {

                        if (err) {
                            console.log("error");
                        }
                        else {
                            if (vr) {
                                vr.in_package = id;
                                console.log("the voucher is ", vr);
                                console.log("the image is ", img);
                                vr.img_path = img;
                                vr.save(function(err, t) {
                                    if (err) {

                                        console.log("error");
                                    }
                                    else {
                                        console.log("correct one");
                                    }
                                });
                            }
                            else {
                                console.log("the error is ", voucher);
                            }
                        }
                    });
                });
            });
        }
        res.json(new Response(true, "correct operation"));
    });
});

router.get('/change_username', function(req, res, next) {
    User.find({}, function(err, users) {
        if (err) {
            res.json(new Error("DB error"));
        }
        else {
            users.forEach(function(u) {
                u.username = u.email;
                u.save(function(err, us) {
                    if (err) {
                        console.log("err");
                    }
                    else {
                        console.log("correct");
                    }
                })
            });
        }
    });
});

router.get('/add_activate_package', function(req, res, next) {
    Package.find({}, function(err, packs) {
        packs.forEach(function(pack) {
            pack.activate = true;
            pack.save(function(err, p) {
                if (err) {
                    console.log("error");
                }
                else {
                    console.log("fjfgjh");
                    console.log("correct");
                }
            })
        })
    })
});

router.get('/set_exp_date', function(req, res, next) {
   User.find({}, function(err, users) {
       if (err) {
           res.json(new Error("DB error"));
       }
       else {
           users.forEach(function(u) {
               if (u.user_type == 'client' && u.client_type != 'guest') {
                    var ex = new Date(Date.now());
                    ex.setFullYear(ex.getFullYear() + 1);
                    u.package_exp_date = ex;
               }
               u.save(function(err, us) {
                   if (err) {
                       console.log("err");
                   }
                   else {
                       console.log("correct");
                   }
               })
           });
       }
   });
});

router.get('/get_client_ip', function(req, res, next) {
    res.json({
        "client_ip_address" : req.connection.remoteAddress
    });
});

router.get('/get_signature/:device_id', function(req, res, next) {
    var deviceId = req.params.device_id;
    var PAY_FORT_SERVICE_COMMAND = "SDK_TOKEN";
    var PAY_FORT_ACCESS_CODE = "BX6n8VOtG27O0rYX29MR";
    var PAY_FORT_MERCHANT_ID = "LXvofYsj";
    var PAY_FORT_LANGUAGE = "en";
    var PAY_FORT_PASSPHRASE = "IMESHAIN";

    var signature = PAY_FORT_PASSPHRASE +
    "access_code=" + PAY_FORT_ACCESS_CODE +
    "device_id=" + deviceId +
    "language=" + PAY_FORT_LANGUAGE +
    "merchant_identifier=" + PAY_FORT_MERCHANT_ID +
    "service_command=" + PAY_FORT_SERVICE_COMMAND +
    PAY_FORT_PASSPHRASE;

    console.log("The sig ==>>>>>>>>>>>>>> \n\n\n");
    console.log(signature);
    console.log("==========>>>>>>>>>>>>>> \n\n\n");
    
    var rp = require('request-promise');
    var sha256 = require('sha256');
    var hashedSig = sha256(signature);

    var options = {
        method: 'POST',
        uri: 'https://paymentservices.payfort.com/FortAPI/paymentApi',
        body: {
            "service_command" : PAY_FORT_SERVICE_COMMAND,
            "access_code" : PAY_FORT_ACCESS_CODE,
            "merchant_identifier" : PAY_FORT_MERCHANT_ID,
            "language" : PAY_FORT_LANGUAGE,
            "device_id" : deviceId,
            "signature" : hashedSig
        },
        json: true // Automatically stringifies the body to JSON
    };
    rp(options)
    .then(function(parsedBody) {
        console.log("here in respnse");
        console.log(hashedSig);
        console.log(parsedBody);
        res.json(new Response(true, "Correct operation", parsedBody));
    })
    .catch(function(err) {
        res.json(new Response(false, "Some error", err));
    })

});
module.exports = router;
