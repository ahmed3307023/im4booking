var express = require('express');
var router = express.Router();
var User = require('../models/user');

var Feedback = require('../models/feedback');
var Error = require('../Utils/error');
var Response = require('../Utils/response');

/* GET home page. */
router.get('/', function(req, res, next) {
    Feedback.find({}, function(err, feedbacks) {
        if(err) res.json(new Error("not valid operation"));
        else {
            res.json(new Response(true, "correct operation", feedbacks));
        }
    })
});

router.post('/delete', function(req, res, next) {
    var feedback_id = req.body.feedback_id;
    Feedback.remove({"_id" : feedback_id}, function(err) {
        if(err) res.json(new Error("Not valid operation"));
        else res.json(new Response(true, "correct operation", null));
    })
});

router.post('/', function(req, res, next) {
    var request_body = req.body;
    var feedback = request_body.feedback;
    var fDB = new Feedback(feedback);
    fDB.save(function(err, feed) {
        if(err) {
            res.json(new Error("DB error"));
        }
        else {
            res.json(new Response(true, "correct operation", feed._id));
        }
    })
})

module.exports = router;
