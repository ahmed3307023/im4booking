var express = require('express');
var router = express.Router();
var User = require('../models/user');
var mongoose = require('mongoose');

var Authentication  = require('../Utils/authentication');
var Helper = require('../Utils/helper');
var reserved_tokens = require('../Strings/reserved_results');
var Package = require('../models/package');
var Offer = require('../models/offer');
var Response = require('../Utils/response');
var Error = require('../Utils/error');

/* GET home page. */
router.get('/', function(req, res, next) {
    Package.find({}, function(err, packages) {
        if(err) {
            res.json(new Error("db error"));
        }
        else {
            res.json(new Response(true, "correct operation", packages));
        }
    })
});

router.post('/', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var package = req.body.package;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            console.log("api checked");
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                console.log("admin checked");
                console.log("the package   ==> ", package);
                var package_data = "";
                try {
                   package_data  = new Package(package);
                }
                catch(e) {
                    console.log(e);
                }
                console.log("here after creating package obj", package);
                package_data.save(function(err, ob) {
                    if(err) {
                        console.log("err ib saving in db ", err);
                        res.json(new Error("db error"));
                    }
                    else {

                        res.json(new Response(true, "correct operation", ob));
                    }
                });
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/edit', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var new_package = req.body.new_package;
    var package_id = req.body.package_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined' && typeof package_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {

                Package.findById(package_id, function(err, package_obj) {
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        for (var field in new_package) {
                            package_obj[field] = new_package[field];
                        }
                        package_obj.save(function(err, r) {
                            if (err) {
                                res.json(new Error("DB error in saving"));
                            }
                            else {
                                res.json(new Response(true, "correct operation", r._id));
                            }
                        });
                    }
                })
            })
            .catch(function(err) {
                res.json(new Error("User not admin"));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/deactivate', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var package_id = req.body.package_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                Package.findById(package_id, function(err, p){
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        p.activate = false;
                        p.save(function(err, pack) {
                            if (err) {
                                res.json(new Error("some error"));
                            }
                            else {
                                res.json(new Response(true, "correct operation", pack));
                            }
                        });
                    }
                });
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/activate', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var package_id = req.body.package_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                Package.findById(package_id, function(err, p){
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        p.activate = true;
                        p.save(function(err, pack) {
                            if (err) {
                                res.json(new Error("some error"));
                            }
                            else {
                                res.json(new Response(true, "correct operation", pack));
                            }
                        });
                    }
                });
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/delete', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var package_id = req.body.package_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                Package.remove({'_id':package_id}, function(err, r){
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        Offer.remove({'in_package': package_id}, function(err, v) {
                            if (err) {
                                res.json(new Error("DB error in remove vouchers in package"));
                            }
                            else {
                                res.json(new Response(true, "correct operation"));
                            }
                        });
                    }
                });
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/voucher', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var package_id = req.body.package_id;
    var voucher = req.body.voucher;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined' && typeof package_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                Package.findById(package_id, function(err, pack) {
                    if (err) res.json(new Error("Package not found"));
                    else {
                        console.log(typeof voucher);
                        var vv = new Offer(voucher);
                        vv.save(function(err, vo) {
                            console.log(err);
                            if (err) res.json(new Error("db error"));
                            else {
                                pack.vouchers.push(vo._id);
                                pack.save(function(err, pp) {
                                    if(err) res.json(new Error("db error 2"));
                                    else {
                                        res.json(new Response(true, "correct operation", vo));
                                    }
                                });
                            }
                        });
                    }
                })
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/voucher/edit', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var new_voucher = req.body.new_voucher;
    var voucher_id = req.body.voucher_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined' && typeof voucher_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                Offer.findById(voucher_id, function(err, voucher_obj) {
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        for (var field in new_voucher) {
                            voucher_obj[field] = new_voucher[field];
                        }
                        voucher_obj.save(function(err, r) {
                            if (err) {
                                res.json(new Error("DB error in saving"));
                            }
                            else {
                                res.json(new Response(true, "correct operation", r._id));
                            }
                        });
                    }
                })
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/voucher/delete', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var voucher_id = req.body.voucher_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                Offer.remove({'_id':voucher_id}, function(err, r){
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        res.json(new Response(true, "correct operation"));
                    }
                });
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.get('/voucher', function(req, res, next) {
    Offer.find({
        is_voucher: true
    }, function(err, vouchers) {
        if(err) {
            res.json(new Error("db error"));
        }
        else {
            Package.find({}, function(err, packs) {
                if (err) {
                    res.json(new Error("DB error"));
                }
                else {
                    var n_packs = packs.map(function(pack) {
                        var v_in_pack = vouchers.filter(function(v) {
                            if (v.in_package.equals(pack._id)) {
                                return v;
                            }
                        });
                        return {
                            "name" : pack.name[0].value,
                            "vouchers" : v_in_pack
                        }
                    });
                    var rr = {
                        'list' : n_packs,
                        'vouchers' : vouchers
                    };
                    res.json(new Response(true, "correct operation", rr));
                }
            })
        }
    })
});

router.get('/voucher/:package_id', function(req, res, next) {
    var package_id = req.params.package_id;
    Package.findById(package_id, function(err, package) {
        if (err) {
            res.json(new Error("Db error"));
        }
        if(package) {
            Helper.get_vouchers(package.vouchers)
            .then(function(vouchers) {
                res.json(new Response(true, "correct operation", vouchers));
            })
            .catch(function(err) {
                res.json(new Error("error"));
            })
        }
        else {
            res.json(new Response(true, "correct operation", []));
        }
    })
});

module.exports = router;
