var express = require('express');
var router = express.Router();

var Error_mapper = require('../Utils/error_mapper');
var Error = require('../Utils/error');
var Authentication  = require('../Utils/authentication');
var Helper = require('../Utils/helper');

var Response = require('../Utils/response')
var Strings = require('../Strings/reserved_results');

var Term = require('../models/term');

router.post('/', function(req, res, next) {
    var t_term = req.body.termsandconditions;
    Term.find({}, function(err, terms) {
        if (err) {
            console.log("error in getting terms");
        }
        else {
            if(terms.length == 0) {
                var t = new Term();
                t.data = t_term;
                console.log("the db object", t);
                t.save(function(err, obj) {
                    if (err){
                        console.log(err);
                        res.json(new Error("some error"));
                    }
                    else res.json(new Response(true, "correct operation", obj));
                });
            }
            else {
                terms[0].data = t_term;
                terms[0].save(function(err, ob) {
                    if (err) res.json(new Error("some other error"));
                    else res.json(new Response(true, "correct operation", ob));
                });
            }
        }
    });
});

router.get('/', function(req, res, next) {
    Term.find({}, function(err, terms) {
        if (err) res.json(new Error("error"));
        else res.json(new Response(true, "correct operation", terms[0]));
    });
});


module.exports = router;

