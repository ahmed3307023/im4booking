var express = require('express');
var router = express.Router();

var User = require('../models/user');
var News = require('../models/news');
var Authentication = require('../Utils/authentication');
var Response = require('../Utils/response');
var Error = require('../Utils/error');
var Helper = require('../Utils/helper')

/* GET home page. */
router.get('/', function(req, res, next) {
    var api_key = req.headers.api_key;
    var since_date = req.headers.since_date;

    Authentication.check_valid_for_all(api_key)
    .then(function() {
        News.find({}, function(err, news) {
            if (err) res.json(new Error("Try again internal error"));
            else {
                news = news.sort(function(a,b) {
                    if (a.date < b.date) {
                        return 1;
                    }
                    else {
                        return -1;
                    }
                });
                res.json(new Response(true,"correct operation", news));
            }
        });
    })
    .catch(function(err) {
        res.json(new Error("invalid api key"));
    })
});

router.post('/', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var news = req.body.news;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            console.log("api checked");
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                news.date = new Date(Date.now());
                var news_data = new News(news);
                news_data.save(function(err, n) {
                    if (err) res.json(new Error("db error"));
                    else {
                        Helper.notifyNews(n)
                        .then(function(r) {
                            res.json(new Response(true, "correct operation", n));
                        })
                        .catch(function() {
                            res.json(new Error("no notification send"));
                        })
                    }
                })
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/delete', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var news_id = req.body.news_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                News.remove({'_id':news_id}, function(err, r){
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        res.json(new Response(true, "correct operation"));
                    }
                });
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/edit', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var new_news = req.body.new_news;
    var news_id = req.body.news_id;
    console.log("the body ", req.body);
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined' && typeof news_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                News.findById(news_id, function(err, news_obj) {
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        console.log();
                        console.log("the news is ", news_obj);
                        console.log();
                        console.log("the edited part is ", new_news);
                        for (var field in new_news) {
                            news_obj[field] = new_news[field];
                        }
                        news_obj.save(function(err, r) {
                            if (err) {
                                res.json(new Error("DB error in saving"));
                            }
                            else {
                                res.json(new Response(true, "correct operation", r._id));
                            }
                        });
                    }
                })
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});
module.exports = router;
