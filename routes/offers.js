var express = require('express');
var router = express.Router();

var Offer = require('../models/offer');
var multiparty = require('connect-multiparty');

var Authentication  = require('../Utils/authentication');
var Helper = require('../Utils/helper');
var reserved_tokens = require('../Strings/reserved_results');

var Response = require('../Utils/response');
var Error = require('../Utils/error');

router.get('/', function(req, res, next) {
    var search_object = {};
    search_object.is_voucher = false;
    Offer.find(search_object, function(err, offers) {
        if(err) {

            console.log("the error is", err);
            res.json(new Error("db error"));
        }
        else res.json(new Response(true, "correct operation", offers));
    });
});

router.get('/:id', function(req, res, next) {
    var id = req.params.id;
    Offer.findById(id, function(err, offer) {
        if(err) res.json(new Error("db error"));
        else res.json(new Response(true, "correct operation", offer));
    });
});

// this is for admin use only to add new offer
router.post('/',function(req, res, next) {
    var request_body = req.body;
    if(typeof request_body.user_id != 'undefined' && typeof request_body.api_key != 'undefined') {
        Authentication.check_admin(request_body.user_id)
        .then(function(admin_data) {
            console.log("checked if user is admin")
            Authentication.api_key_check(admin_data.user_type, request_body.api_key)
            .then(function(api_key) {
                console.log("here in user authenticated")
                var offer_data = request_body.offer;
                if(typeof offer_data.name != 'undefined' && typeof offer_data.description != 'undefined'
                    && typeof offer_data.condition_type != 'undefined') {
                        console.log(request_body);
                        var offer_data = request_body.offer;

                        var o = new Offer(offer_data);
                        o.save(function(err, o_save) {
                            if (err) {
                                console.log(err);
                                res.json(new Error("not valid operation"));
                            }
                            else {
                                console.log("valid operation")
                                res.json(new Response(true, "correct operation", o_save));
                            }
                        });
                }
                else {
                    res.json(new Error("insufficient data"));
                }
            })
            .catch(function() {
                res.json(new Error("invalid api key"));
            })
        })
        .catch(function() {
            res.json(new Error("not authorized user"));
        })
    }
    else {
        res.json(new Error("user id or api key not found"));
    }
});

// this is for admin use only to edit the offer data
router.post('/edit', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var new_offer = req.body.new_offer;
    var offer_id = req.body.offer_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined' && typeof offer_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                Offer.findById(offer_id, function(err, offer_obj) {
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        if(offer_obj) {
                            for (var field in new_offer) {
                                console.log(field, "  ", new_offer[field]);
                                offer_obj[field] = new_offer[field];
                            }
                            offer_obj.save(function(err, r) {
                                if (err) {
                                    console.log(err);
                                    res.json(new Error("DB error in saving"));
                                }
                                else {
                                    res.json(new Response(true, "correct operation", r._id));
                                }
                            });
                        }
                        else {
                            console.log("no offer found");
                            res.json(new Error("no offer"));
                        }
                    }
                })
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
});

router.post('/delete', function(req, res, next) {
    var api_key = req.body.api_key;
    var user_id = req.body.user_id;
    var offer_id = req.body.offer_id;
    if (typeof api_key != 'undefined' && typeof user_id != 'undefined') {
        Authentication.api_key_check("admin", api_key)
        .then(function(api) {
            Authentication.check_admin(user_id)
            .then(function(user_data) {
                Offer.remove({'_id':offer_id}, function(err, r){
                    if (err) {
                        res.json(new Error("DB error"));
                    }
                    else {
                        res.json(new Response(true, "correct operation"));
                    }
                });
            })
            .catch(function(err) {
                res.json(new Error(new Error("error")));
            })
        })
        .catch(function(err) {
            res.json(new Error("invalid api key"));
        })
    }
    else {
        res.json(new Error("data not sufficient"));
    }
})
module.exports = router;


